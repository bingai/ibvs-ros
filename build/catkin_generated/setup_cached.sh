#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export CATKIN_TEST_RESULTS_DIR="/home/bing/catkin_ws/build/test_results"
export ROS_TEST_RESULTS_DIR="/home/bing/catkin_ws/build/test_results"

# modified environment variables
export CMAKE_PREFIX_PATH="/home/bing/catkin_ws/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/bing/catkin_ws/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/bing/catkin_ws/devel/lib:$LD_LIBRARY_PATH"
export PATH="/home/bing/catkin_ws/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/bing/catkin_ws/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/bing/catkin_ws/build"
export PYTHONPATH="/home/bing/catkin_ws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/bing/catkin_ws/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/bing/catkin_ws/src:/opt/ros/hydro/share:/opt/ros/hydro/stacks"