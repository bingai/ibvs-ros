/**
@class IO_ToolBox_Eigen
@brief I/O functions base on Eigen
- Version v1.0
- Function List:
 -# read_txt
 -# write_txt
 -# cast_comma
- Modify Record��
-# v1.0-2013-4-25-wudan
*/

#include "toolbox/IO_ToolBox_Eigen.h"

using namespace std;
using namespace Eigen;

/**
@fn IO_ToolBox_Eigen::IO_ToolBox_Eigen(void)
@brief  Initionlize class
- Modify Record��
-# v1.0-203-4-25-wudan
*/
IO_ToolBox_Eigen::IO_ToolBox_Eigen(void)
{
}
/**
@fn IO_ToolBox_Eigen::~IO_ToolBox_Eigen(void)
@brief  Decompose class
- Modify Record��
-# v1.0-203-4-25-wudan
*/

IO_ToolBox_Eigen::~IO_ToolBox_Eigen(void)
{
}

/**
@fn int IO_ToolBox_Eigen::read_txt(std::string adress,Eigen::MatrixXd &M,int row,int col)
@brief read data in text
- Parameter List:
-# string adress, the adress of text
-# MatrixXd M,    matrix we need
-# int row,       the row of matrix
-# int col,       the col of matrix
- Dependencies��
-# Eigen
- Modify Record��
-# v1.0-2013-4-25-wudan
*/


int IO_ToolBox_Eigen::read_txt(std::string adress,Eigen::MatrixXd &M,int row,int col)
{
	ifstream read_file ( adress.c_str());

	if(!read_file)
		cout<<adress.c_str()<<" is not exist!"<<endl;

	string line,item;

	MatrixXd M_item(row,col);
	for ( int i = 0; i < row; i++ )
	{
		//Get data of every line
		getline ( read_file, line );

		cast_comma( line );
		istringstream linestream( line );

		for ( int  j = 0; j < col; j++ )
		{
			linestream>>item;
			M_item(i,j) = atof ( item.c_str() );
		}

	}
	M=M_item;

	read_file.close();

	return 1;
}


int IO_ToolBox_Eigen::read_txt(std::string adress, Eigen::MatrixXd &M)
{
	ifstream read_file ( adress.c_str());

	if(!read_file.is_open())
	{
		std::cout <<adress.c_str()<<"   file is not existed!"<<endl;
	}

	string line,item;
	//calculate row col of data
	int row = 1,col = 0;
	getline ( read_file, line );
	cast_comma( line );
	istringstream linestream_t( line );

	while(linestream_t>>item)
	{
		++col;
	}

	while( getline ( read_file, line ) )
	{
		row++;
	}
	//calculate end

	read_file.close();
	read_file.open(adress.c_str());
	MatrixXd M_item(row,col);
	for ( int i = 0; i < row; i++ )
	{
		//Get data of every line
		getline ( read_file, line );

		cast_comma( line );
		istringstream linestream( line );

		for ( int  j = 0; j < col; j++ )
		{
			linestream>>item;
			M_item(i,j) = atof ( item.c_str() );
		}

	}

	M=M_item;

	read_file.close();

	return 1;
}
/**
@fn int IO_ToolBox_Eigen::read_txt(std::string adress,Eigen::MatrixXf &M,int row,int col)
@brief read data in text
- Parameter List:
-# string adress, the adress of text
-# MatrixXf M,    matrix we need
-# int row,       the row of matrix
-# int col,       the col of matrix
- Dependencies��
-# Eigen
- Modify Record��
-# v1.0-2013-4-25-wudan
*/
int IO_ToolBox_Eigen::read_txt(std::string adress,Eigen::MatrixXf &M,int row,int col)
{
	ifstream read_file ( adress.c_str());

	if(!read_file)
		cout<<adress.c_str()<<"    is not exist!"<<endl;

	string line,item;

	MatrixXf M_item(row,col);
	for ( int i = 0; i < row; i++ )
	{
		//Get data of every line
		getline ( read_file, line );

		cast_comma( line );
		istringstream linestream( line );

		for ( int  j = 0; j < col; j++ )
		{
			linestream>>item;
			M_item(i,j) = atof ( item.c_str() );
		}

	}
	M=M_item;

	read_file.close();

	return 1;
}
/**
@fn int IO_ToolBox_Eigen::write_txt(std::string adress,Eigen::MatrixXd sur)
@brief save data in text
- Parameter List:
-# string adress, the adress of text
-# MatrixXd sur,    matrix we need to save
- Dependencies��
-# Eigen
- Modify Record��
-# v1.0-2013-4-25-wudan
*/
int IO_ToolBox_Eigen::write_txt(std::string adress,Eigen::MatrixXd sur,write_mode mode )
{
	ofstream write;

	switch (mode)
	{
	case 0:
		{
			write.open(adress.c_str(), ofstream::out);
			break;
		}
	case 1:
		{
			write.open(adress.c_str(), ofstream::app);
			write.seekp( ofstream::end);
			//write<<endl;
			break;
		}

	}
	if(!write)
		cout<<"Can not build the txt!"<<endl;

	write.precision( 18 );

	for( int i = 0;i < sur.rows();++i)
	{
		for( int j = 0;j < sur.cols();++j)
		{
			write<<sur(i,j)<<" ";

		}
		write<<endl;
	}

	write.close();

	return 1;
}
/**
@fn int IO_ToolBox_Eigen::write_txt(std::string adress,Eigen::MatrixXf sur)
@brief save data in text
- Parameter List:
-# string adress, the adress of text
-# MatrixXf sur,    matrix we need to save
- Dependencies��
-# Eigen
- Modify Record��
-# v1.0-2013-4-25-wudan
*/
int IO_ToolBox_Eigen::write_txt(std::string adress,Eigen::MatrixXf sur)
{
	ofstream out(adress.c_str());
	if(!out)
		cout<<"Can not build the txt!"<<endl;

	out.precision( 18 );

	for( int i = 0;i < sur.rows();++i)
	{
		for( int j = 0;j < sur.cols();++j)
		{
			out<<sur(i,j)<<" ";

		}
		out<<endl;
	}

	out.close();

	return 1;
}
/**
@fn int IO_ToolBox_Eigen::write_txt(std::string adress,double * input_array,int length)
@brief save data in text
- Parameter List:
-# string adress, the adress of text
-# double * input_array, data we need to save
-# int length, the length of data
- Modify Record��
-# v1.0-2013-4-25-wudan
*/
int IO_ToolBox_Eigen::write_txt(std::string adress,double * input_array,int length)
{
	ofstream output(adress.c_str());
	if(!output)
		cout<<"Can not build the txt!"<<endl;

	output.precision( 18 );
	cout<<"output"<<endl;
	for(int i=0;i<length; ++i)
	{
		output<<input_array[i]<<" ";
		output<<endl;
	}
	cout<<"output"<<endl;
	return 1;
}
/**
@fn IO_ToolBox_Eigen::write_txt(std::string adress,int * input_array,int length)
@brief save data in text
- Parameter List:
-# string adress, the adress of text
-# int * input_array, data we need to save
-# int length, the length of data
- Modify Record��
-# v1.0-2013-4-25-wudan
*/
int IO_ToolBox_Eigen::write_txt(std::string adress,int * input_array,int length)
{
	ofstream out(adress.c_str());
	if(!out)
		cout<<"Can not build the txt!"<<endl;

	out.precision( 18 );

	for(int i=0;i<length; ++i)
	{
		out<<input_array[i]<<" ";
		out<<endl;
	}

	return 1;
}
/**
@fn IO_ToolBox_Eigen::cast_comma(std::string &s)
@brief cast comma in string
- Parameter List:
-# string s, the source string
- Modify Record��
-# v1.0-2013-4-25-wudan
*/
int IO_ToolBox_Eigen::cast_comma(std::string &s)
{
	size_t pos;
	while((pos = s.find(",")) != string::npos)
	{
		s.replace(pos, 1, " ");
	}

	return 1;
}

bool IO_ToolBox_Eigen::SaveNum(const std::string filename, const int val)
{
    std::ofstream ofs (filename.c_str());
    if(ofs.is_open())
    {
        ofs<< val;
        ofs.close();
    }
    else
    {
        std::cout << filename <<" cannot be opened, @SaveNum funtion " <<std::endl;
        return false;
    }
    return true;
}

