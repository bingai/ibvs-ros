/**
@class Math_ToolBox
@brief some math library
- Version v1.0
- Function List:
 -# convertFromHomo
 -# convertToHomo
 -# rodrigues
- Modify Record��
-# v1.0-2013-4-25-wudan
*/
#include "toolbox/Math_ToolBox.h"

using namespace Eigen;
using namespace std;
/**
@fn Math_ToolBox::Math_ToolBox(void)
@brief  Initionlize class
- Modify Record��
-# v1.0-203-4-25-wudan
*/
Math_ToolBox::Math_ToolBox(void)
{
}
/**
@fn Math_ToolBox::~Math_ToolBox(void)
@brief  Decompose class
- Modify Record��
-# v1.0-203-4-25-wudan
*/

Math_ToolBox::~Math_ToolBox(void)
{
}
/**
@fn MatrixXd Math_ToolBox::convertFromHomo(Eigen::MatrixXd sur,HomoMethod method)
@brief  convert homography point to normal point
- Parameter List:
 -# MatrixXd sur, the matrix need to convert
 -# HomoMethod method, method=0,convert row,method=1,convert col
- Dependencies��
-# Eigen
- Modify Record��
-# v1.0-203-4-25-wudan
*/

Eigen::MatrixXd Math_ToolBox::
pinv(const Eigen::MatrixXd &M, const double tol)
{
	Eigen::MatrixXd pinvmat;
	Eigen::JacobiSVD<Eigen::MatrixXd> svd(M, Eigen::ComputeFullU | Eigen::ComputeFullV);
	Eigen::VectorXd MS = svd.singularValues();
    	Eigen::MatrixXd MU = svd.matrixU();
    	Eigen::MatrixXd MV = svd.matrixV();
    	Eigen::RowVectorXd MS_inv = MS;

    //cout << "MS_inv = " << endl << MS_inv << endl;

	int I_max = M.cols();
	if (M.rows() < M.cols()){
		I_max = M.rows();
	}


	for ( int i=0; i<I_max; ++i) {
		if ( MS(i) > tol )
		MS_inv(i)=1.0/MS(i);
		else MS_inv(i)=0;
	}
	//cout << "MS_inv = " << endl << MS_inv << endl;


	if (M.rows() < M.cols()){
		Eigen::MatrixXd mV = MV.block(0,0,M.cols(),MS.size());
		//cout << "mV = " << endl << mV << endl;
		pinvmat = mV*MS_inv.asDiagonal()*MU.transpose();
	}

    else{
    	Eigen::MatrixXd mAdjointU = MU.adjoint().block(0,0,MS.size(),M.rows());
    	pinvmat = MV*MS_inv.asDiagonal()*mAdjointU;

    }

   	return pinvmat;
}

MatrixXd Math_ToolBox::convertFromHomo(Eigen::MatrixXd sur,HomoMethod method)
{
/******From homogeneous,all we need to do is just divide a row(col)*****************/

	if(method==HOMO_ROW) // 4,n or 3,n
	{
	    assert ( (sur.rows() == 4) || (sur.rows() == 3 )  );
		MatrixXd Sur_sub(sur.rows()-1,sur.cols());

		for(int i=0;i<sur.rows()-1;++i)
		{
			Sur_sub.row(i)=sur.row(i).array()/sur.row(sur.rows()-1).array();
		}
		return Sur_sub;

	}
	if(method==HOMO_COL) //n,4 or n,3
	{
        assert ( (sur.cols() == 4) || (sur.cols() == 3 )  );
		MatrixXd Sur_sub(sur.rows(),sur.cols()-1);

		for(int i=0;i<sur.cols()-1;++i)
		{
			Sur_sub.col(i)=sur.col(i).array()/sur.col(sur.cols()-1).array();
		}

		return Sur_sub;
	}

}
/**
@fn MatrixXf Math_ToolBox::convertFromHomo(Eigen::MatrixXf sur,HomoMethod method)
@brief  convert homography point to normal point

  - Parameter List:
     -# MatrixXf sur, the matrix need to convert
     -# HomoMethod method, method=0,convert row,method=1,convert col

  - Dependencies:
     -# Eigen

  - Modify Record��
     -# v1.0-203-4-25-wudan
*/

MatrixXf Math_ToolBox::convertFromHomo(Eigen::MatrixXf sur,HomoMethod method)
{
/******From homogeneous,all we need to do is just divide a row(col)*****************/

	if(method==HOMO_ROW)
	{
	    assert ( (sur.rows() == 4) || (sur.rows() == 3 )  );
		MatrixXf Sur_sub(sur.rows()-1,sur.cols());

		for(int i=0;i<sur.rows()-1;++i)
		{
			Sur_sub.row(i)=sur.row(i).array()/sur.row(sur.rows()-1).array();
		}
		return Sur_sub;

	}
	if(method==HOMO_COL)
	{
	    assert ( (sur.cols() == 4) || (sur.cols() == 3 )  );

		MatrixXf Sur_sub(sur.rows(),sur.cols()-1);

		for(int i=0;i<sur.cols()-1;++i)
		{
			Sur_sub.col(i)=sur.col(i).array()/sur.col(sur.cols()-1).array();
		}

		return Sur_sub;
	}

}
/**
@fn MatrixXd Math_ToolBox::convertToHomo(Eigen::MatrixXd sur,HomoMethod method)
@brief  convert homography point to normal point

  - Parameter List:
     -# MatrixXf sur, the matrix need to convert
     -# HomoMethod method, method=0,convert row,method=1,convert col

  - Dependencies:
     -# Eigen

  - Modify Record��
     -# v1.0-203-4-25-wudan
*/
MatrixXd Math_ToolBox::convertToHomo(Eigen::MatrixXd sur,HomoMethod method)
{
/******To homogeneous,all we need to do is just add a row(col),elements is 1**********/

	if(method==HOMO_ROW)
	{
	     assert ( (sur.rows() == 2) || (sur.rows() == 3 )  );
		ArrayXXd add(1,sur.cols());
	    add.row(0)=ArrayXd::LinSpaced(sur.cols(),1,1);
		MatrixXd dis(sur.rows()+1,sur.cols());
		for(int i=0;i<sur.rows();++i)
		{
			dis.row(i)=sur.row(i);
		}
		dis.row(sur.rows())=add;

		return dis;
	}
	if(method==HOMO_COL)
	{
	    assert ( (sur.cols() == 2) || (sur.cols() == 3 )  );
		ArrayXXd add(sur.rows(),1);
	    add.col(0)=ArrayXd::LinSpaced(sur.rows(),1,1);
		MatrixXd dis(sur.rows(),sur.cols()+1);
		for(int i=0;i<sur.cols();++i)
		{
			dis.col(i)=sur.col(i);
		}
		dis.col(sur.cols())=add;

		return dis;
	}
}


/**
@fn MatrixXf Math_ToolBox::convertToHomo(Eigen::MatrixXf sur,HomoMethod method)
@brief convert normal point to homography point
- Parameter List:
 -# MatrixXf sur, the matrix need to convert
 -# HomoMethod method, method=0,convert row,method=1,convert col
- Dependencies��
-# Eigen
- Modify Record��
-# v1.0-203-4-25-wudan
*/
MatrixXf Math_ToolBox::convertToHomo(MatrixXf sur,HomoMethod method)
{
/******To homogeneous,all we need to do is just add a row(col),elements is 1**********/
	/*Mat dis_cv;
	MatrixXd Sur(sur.rows,sur.cols);
	cv2eigen(sur,Sur);*/
	if(method==HOMO_ROW)
	{
	     assert ( (sur.rows() ==2) || (sur.rows() == 3 )  );
		ArrayXXf add(1,sur.cols());
	    add.row(0)=ArrayXf::LinSpaced(sur.cols(),1,1);
		MatrixXf dis(sur.rows()+1,sur.cols());
		for(int i=0;i<sur.rows();++i)
		{
			dis.row(i)=sur.row(i);
		}
		dis.row(sur.rows())=add;

		return dis;
	}
	if(method==HOMO_COL)
	{
	    assert ( (sur.cols() == 2) || (sur.cols() == 3 )  );
		ArrayXXf add(sur.rows(),1);
	    add.col(0)=ArrayXf::LinSpaced(sur.rows(),1,1);
		MatrixXf dis(sur.rows(),sur.cols()+1);
		for(int i=0;i<sur.cols();++i)
		{
			dis.col(i)=sur.col(i);
		}
		dis.col(sur.cols())=add;

		return dis;
	}
}
/**
@fn void Math_ToolBox::rodrigues( Eigen::MatrixXd sur, Eigen::MatrixXd &dis )
@brief convert rotation vector to rotation matrix, vice versa
- Parameter List:
-# MatrixXd sur, matrix need to convert
-# MatrixXd dis, matrix after convert
- Dependencies��
-# Eigen
- Modify Record��
-# v1.0-203-4-25-wudan
*/
void  Math_ToolBox::rodrigues( Eigen::MatrixXd sur, Eigen::MatrixXd &dis )//zhz:�����
{
	int m,n;
	m = sur.rows(),n = sur.cols();

	double theta;
	double ep = numeric_limits<double>::epsilon();
	double bigeps = 10e+20*ep;
	bool term1,term2;
	//the term need next
	if( ( m==3 ) && ( n==3 ) )
	{
	   term1 = ( sur.transpose() * sur - MatrixXd::Identity(3,3) ).norm() < bigeps;
	   term2 = abs(sur.determinant()-1)< bigeps;
	}

	MatrixXd R;
	MatrixXd omega ;
	double alpha,beta,gamma;

	MatrixXd omegav(3,3),A;


	//it is a rotation vector
	if( ((m==3)&&(n==1))||((m==1)&&(n==3)) )
	{
		theta = sur.norm();
		if( theta < ep )
		{
			R.setIdentity(3,3);

		}//m = sur.rows(),n = sur.cols();
		else if( ( (n==sur.rows())&&(sur.rows()>sur.cols()) )  ||  ( (n==sur.cols())&&(sur.rows()<sur.cols()) ) )
				//make it a column vec. if necess.
			{
				omega = sur.transpose()/theta;
			}

		else
			{
				omega = sur/theta;
		    }
		alpha = cos( theta );
		beta  = sin( theta );
		gamma = 1-cos( theta );

		omegav<<    0    ,  -omega(2), omega(1),
			    omega(2) , 0        , -omega(0),
		        -omega(1), omega(0) ,   0;

		  A = omega*omega.transpose();
		  R = MatrixXd::Identity(3,3)*alpha + omegav*beta + A*gamma;
		dis = R;

	}

	else if( (m==n)&&(m==3)&&term1&&term2 )
	{
		JacobiSVD<MatrixXd> svd(sur, ComputeFullU | ComputeFullV);
	    MatrixXd V,U;
	    V = svd.matrixV();
	    U = svd.matrixU();
		R = U*V.transpose();

		double tr;
		tr = (R.trace()-1)/2;

		if(tr>1)
			cout<<"acos error!"<<endl;

        theta = acos(tr);

		if(sin(theta) >= 1e-4)
		{
			double vth;
			vth = 1/(2*sin(theta));
			MatrixXd om1_item(1,3),om1;
			om1_item<<R(2,1)-R(1,2), R(0,2)-R(2,0), R(1,0)-R(0,1);
			om1 = om1_item.transpose();

			MatrixXd om;
			om = vth*om1;
			dis= om*theta;


		}
		else if(tr>0)
		{
			MatrixXd  dis_item(3,1);
			dis_item<<0,0,0;
			dis = dis_item;
		}


	}
}
/**
@fn Matrix3d skew_matrix(Vector3d vec)
@brief compute skew matrix based a 3 d vector.
- Parameter List:
-# Vector3d vec
- Dependencies��
-# Eigen
- Modify Record��
-# 2014-1-15-MaLin,Terence Li
*/
Matrix3d Math_ToolBox::skew_matrix( const Vector3d vec)
{

Matrix3d sk;

sk << 	0, -vec(2), vec(1),
 	vec(2), 0, -vec(0),
	-vec(1), vec(0), 0;

return sk;
}

Eigen::MatrixXd
Math_ToolBox::matrix_minor(const Eigen::MatrixXd &M,  int row, int col)
{

    int rows, cols, count_row, count_col;

    rows = M.rows();
    cols = M.cols();

    assert( rows == cols ); // input must be square matrix

    Eigen::MatrixXd M_minor(rows-1, cols-1);

    count_row =0;
    for (int r=0; r<rows; r++){
        if (r != row){
            count_col =0;
            for (int c=0; c<cols; c++){
                if (c != col){
                    M_minor(count_row, count_col) = M(r,c);
                    count_col++;
                }
            }
        count_row++;
        }
    }
    return M_minor;
}


Eigen::MatrixXd
Math_ToolBox::matrix_cofactor(const Eigen::MatrixXd &M)
{

int rows, cols;

// Get the size of input matrix
rows = M.rows();
cols = M.cols();

assert(rows == cols); // input must be square matrix

// Define varable to store the matrix minor and matrix cofactor
Eigen::MatrixXd M_minor;
Eigen::MatrixXd M_cof(rows, cols);


for (int r = 0; r<rows; r++){
	for (int c = 0; c<cols; c++){
		M_minor = matrix_minor(M,r,c);
		M_cof(r,c) = pow(-1, r+c)*M_minor.determinant();
	}
}
return M_cof;
}

/************************************************
// Select the specific cols or rows in a matrix
// or matrix block with specific rows and cols
// Matrix input with double type
// Vector input with integer type
// example: if A = 	[a b c d]
//					[e f g h]
//					[i j k l]
//					[m n o p]
// 		  select = [1, 3]
// A_select = multiRows(A, select);
// 			= 	[a b c d]
// 				[i j k l]
// A_select = multiCols(A, select);
// 			= 	[a c]
// 			 	[e g]
// 				[i k]
// 				[m o]
// A_select = anyBlock(A, select, select);
// 			= 	[a c]
// 			 	[i j]
// By Terence Li
// PI Electronics (H.K.) Ltd
// 10 Feb 2014
*************************************************/

/*
Eigen::MatrixXd
multiRows(const Eigen::MatrixXd &A, const std::vector <int> selected_rows_ZeroBased)
{
	int cols = A.cols();
	int length = selected_rows_ZeroBased.size();

	Eigen::MatrixXd A_select(length, cols);

	for (int k=0; k<length; k++){
		A_select.row(k) = A.row( selected_rows_ZeroBased[k] );
	}

	return A_select;
}


Eigen::MatrixXd
multiCols(const Eigen::MatrixXd &A, const std::vector <int> selected_cols_ZeroBased)
{

	int rows = A.rows();
	int length = selected_cols_ZeroBased.size();

	Eigen::MatrixXd A_select(rows, length);

	for (int k=0; k<length; k++){
		A_select.col(k) = A.col(selected_cols_ZeroBased[k]);
	}
	return A_select;
}


Eigen::MatrixXd
anyBlock(const Eigen::MatrixXd &A,  const std::vector <int> selected_rows_ZeroBased ,const std::vector <int> selected_cols_ZeroBased)
{
	Eigen::MatrixXd A_select_rows = multiRows(A, selected_rows_ZeroBased);
	Eigen::MatrixXd A_select(A_select_rows, selected_cols_ZeroBased);

	return A_select;
}
*/
