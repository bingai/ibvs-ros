#include "toolbox/VisGeo.h"

using namespace Eigen;
using namespace std;

double VisGeo_PI::deg2rad (const double deg)
{
	double TWO_PI_DIV_360 = 0.017453292519943;
	return ( deg * TWO_PI_DIV_360 );
}

double VisGeo_PI::calc_cos_sin (const double angle_deg, double & cosA,double & sinA)
{
	double angle_rad = deg2rad( angle_deg );
	 cosA = std::cos(angle_rad);
	 sinA = std::sin(angle_rad);
	return angle_rad;

}

bool VisGeo_PI::gen_RotationMatrix (const double angle_deg, Eigen::MatrixXd & rotation_matrix, VisGeo_PI::RotationMatrixSelection key )
{
	Eigen::MatrixXd R_mat =Eigen::MatrixXd::Identity(3,3);
	double cosA,sinA;
	calc_cos_sin( angle_deg, cosA, sinA );
	switch (key)
	{
		case RX:
			R_mat(1,1) = cosA;
			R_mat(1,2) = -sinA;
			R_mat(2,1) = sinA;
			R_mat(2,2) = cosA;
			break;
		case RY:
			R_mat(0,0) = cosA;
			R_mat(2,0) = -sinA;
			R_mat(0,2) = sinA;
			R_mat(2,2) = cosA;
			break;
		case RZ:
			R_mat(0,0) = cosA;
			R_mat(0,1) = -sinA;
			R_mat(1,0) = sinA;
			R_mat(1,1) = cosA;
			break;
		default:
			std::cout << "VisGeo_PI::gen_RotationMatrix -> input key Error, should be RX,RY or RZ <<" <<std::endl;
			return false;
	}
	rotation_matrix = R_mat;
	return true;
}
/*
bool VisGeo_PI::gen_RotMat_3D (const double angleX_deg,const double angleY_deg,const double angleZ_deg,
						      Eigen::MatrixXd & rotation_matrix)
{
	Eigen::MatrixXd Rx_mat =Eigen::MatrixXd::Identity(3,3);
	Eigen::MatrixXd Ry_mat =Eigen::MatrixXd::Identity(3,3);
	Eigen::MatrixXd Rz_mat =Eigen::MatrixXd::Identity(3,3);
	Eigen::MatrixXd R3d_mat =Eigen::MatrixXd::Identity(3,3);
	assert ( gen_RotationMatrix (angleX_deg,Rx_mat,RX) );
	assert ( gen_RotationMatrix (angleY_deg,Ry_mat,RY) );
	assert ( gen_RotationMatrix (angleZ_deg,Rz_mat,RZ) );

	R3d_mat = Rz_mat * Ry_mat * Rx_mat ;
	rotation_matrix = R3d_mat;
	return true;
}
*/

bool VisGeo_PI::gen_RotMat_3D (const double angleX_deg,const double angleY_deg,const double angleZ_deg,
						      Eigen::MatrixXd & rotation_matrix)
{
	Eigen::MatrixXd Rx_mat =Eigen::MatrixXd::Identity(3,3);
	Eigen::MatrixXd Ry_mat =Eigen::MatrixXd::Identity(3,3);
	Eigen::MatrixXd Rz_mat =Eigen::MatrixXd::Identity(3,3);
	Eigen::MatrixXd R3d_mat =Eigen::MatrixXd::Identity(3,3);
	assert ( gen_RotationMatrix (angleZ_deg,Rx_mat,RX) );
	assert ( gen_RotationMatrix (angleY_deg,Ry_mat,RY) );
	assert ( gen_RotationMatrix (angleX_deg,Rz_mat,RZ) );

	R3d_mat = Rz_mat * Ry_mat * Rx_mat ;
	rotation_matrix = R3d_mat;
	return true;
}


/**********************************

PoseVect: 6-by-1
X Y Z alpha, deta, gamma
alpha, deta, gamma are rotx,roty and rotz

Transformation is 4-by-4

*********************************/

bool
VisGeo_PI::Pose2T(const Eigen::MatrixXd PoseVect, Eigen::MatrixXd & Transformation )
{
	//check dim of input
	assert ( (PoseVect.rows() == 6) && (PoseVect.cols() == 1) );
	// PoseVect's angle must be degree, NOT rad.

	Eigen::MatrixXd R_mat =Eigen::MatrixXd::Identity(3,3);
	Eigen::MatrixXd T_mat =Eigen::MatrixXd::Identity(4,4);

	///generate rotation matrix from a,b,g angle in deg
	gen_RotMat_3D (PoseVect(3,0),PoseVect(4,0),PoseVect(5,0), R_mat);

	T_mat.block<3,3>(0,0) = R_mat;
	T_mat.block<3,1> (0,3) = PoseVect.block<3,1>(0,0);

	Transformation = T_mat;

	return true;
}

double VisGeo_PI::rad2deg (const double rad)
{
	double const_180_DIV_PI = 57.295779513082320;
	return ( rad * const_180_DIV_PI );
}

bool VisGeo_PI:: T2Pose ( const  Eigen::MatrixXd & Transformation,  Eigen::MatrixXd & PoseVect)
{
	MatrixXd  PoseVect_rtn(6,1);

double angy = std::atan2 (  -Transformation(2,0),  std::sqrt(  Transformation(0,0) * Transformation(0,0) + Transformation(1,0) * Transformation(1,0))  );
double cos_y = std::cos(angy);
double angz = std::atan2 (  Transformation(1,0)/cos_y,  Transformation(0,0)/cos_y  );
double angx = std::atan2 (  Transformation(2,1)/cos_y,  Transformation(2,2)/cos_y  );

PoseVect_rtn(0,0) = Transformation(0,3);
PoseVect_rtn(1,0) =  Transformation(1,3);
PoseVect_rtn(2,0) =  Transformation(2,3);
/*
PoseVect_rtn(3,0) = rad2deg ( angx  );
PoseVect_rtn(4,0) = rad2deg ( angy  );
PoseVect_rtn(5,0) = rad2deg ( angz  );
*/
PoseVect_rtn(3,0) = rad2deg ( angz  );
PoseVect_rtn(4,0) = rad2deg ( angy  );
PoseVect_rtn(5,0) = rad2deg ( angx  );

PoseVect = PoseVect_rtn;

//cout << "PoseVect"<<endl<<PoseVect<<endl;
return true;

}

bool VisGeo_PI::TransformPoints ( const Eigen::MatrixXd T_matrix, const Eigen::MatrixXd PointsMat_byCol,
                          Eigen::MatrixXd & resultPointsMat )
{
    /// T must be square Matrix
    bool check_T_sq =  (T_matrix.cols() ==T_matrix.cols() );
    assert ( check_T_sq == true);

    /// pointsMat shoule be 3-by-n or 2-by-n AND T must be 4-by-4 or 3-by-3 depends on points
    int point_number = PointsMat_byCol.cols();
    int point_dim = PointsMat_byCol.rows();
    bool check_pointDim_good =      (T_matrix.cols() == point_dim+1)        &    (  (point_dim== 2)      ||    ( point_dim  == 3)   )   ;
    assert (check_pointDim_good == true);
    //  ------- chech end -----//

    // init data
    MatrixXd resultMat =  Eigen::MatrixXd::Ones(point_dim, point_number);
    MatrixXd PointsMat_homo  ( point_dim+1, point_number );
    MatrixXd result_homo ( point_dim+1, point_number   );

    // computation
   PointsMat_homo  = mt.convertToHomo(PointsMat_byCol,Math_ToolBox::HOMO_ROW);  //HOMO_ROW means last row will be 1
   result_homo = T_matrix * PointsMat_homo;
   resultMat  = mt.convertFromHomo(result_homo,Math_ToolBox::HOMO_ROW);

    //return
    resultPointsMat = resultMat;

    return true;
}

bool VisGeo_PI::ProjectToImagePlane ( const Eigen::Matrix3d Intrinsic, const Eigen::MatrixXd PointsMat_byCol,
                          Eigen::MatrixXd & ImagePoints )
{
    int point_dim = PointsMat_byCol.rows();
    int point_number = PointsMat_byCol.cols();
     assert ( point_dim == 3);
    MatrixXd  ImagePoints_output =  Eigen::MatrixXd::Zero(2, point_number);
    MatrixXd result_homo =  Eigen::MatrixXd::Zero(3, point_number);

    result_homo = Intrinsic * PointsMat_byCol;
    ImagePoints_output  = mt.convertFromHomo(result_homo,Math_ToolBox::HOMO_ROW);

    ImagePoints = ImagePoints_output;
    return true;

}

bool VisGeo_PI::BackProject (  const Eigen::Matrix3d Intrinsic_inv,   const Eigen::MatrixXd ImagePoints,    Eigen::MatrixXd & project_lines )
{
    int point_dim = ImagePoints.rows();
    int point_number = ImagePoints.cols();
	assert ( point_dim == 2);
	MatrixXd  ImagePoints_homo =  Eigen::MatrixXd::Zero(3, point_number);
	 MatrixXd result_homo =  Eigen::MatrixXd::Zero(3, point_number);

	ImagePoints_homo = mt.convertToHomo(ImagePoints,Math_ToolBox::HOMO_ROW );
	result_homo = Intrinsic_inv * ImagePoints_homo;
	project_lines = result_homo;

	return true;
}

bool VisGeo_PI::FromTwoCamerasToRobot_3D ( Eigen::MatrixXd & Points3d_Robot, const Eigen::MatrixXd &T_cr_A, const Eigen::MatrixXd &T_cr_B,
                               const Eigen::MatrixXd  &Points3d_Cam_A, const Eigen::MatrixXd  &Points3d_Cam_B){

        Eigen::MatrixXd temp_A, temp_B;

        TransformPoints(T_cr_A, Points3d_Cam_A, temp_A);
        TransformPoints(T_cr_B, Points3d_Cam_B, temp_B);
        //cout << "temp_a = " <<endl << temp_a << endl;
        //cout << "temp_b = " << endl << temp_b <<endl;
        if ((temp_A-temp_B).norm() < 1e-6)
            Points3d_Robot = temp_A;
        else{
            cout << "The calculated points for both cameras are not exactly the same, there avaerge are used!" << endl;
            Points3d_Robot = (temp_A+temp_B)/2;
        }

        return true;
}



