#include "../Include/toolbox/Convertor_ToolBox.h"


using Eigen::MatrixXd;
using std::vector;

vector<vector<double > >  Convertor_ToolBox::
Eigen2vector(const Eigen::MatrixXd & objPts_in)
{
	vector<vector<double > >  objPts_out;
	for(int i=0;i< objPts_in.rows();i++)
	{
		vector<double> objt;
		for(int j=0;j<objPts_in.cols();j++)
		{
			objt.push_back(objPts_in(i,j));
		}
			objPts_out.push_back(objt);
	}
	return objPts_out;
}


Eigen::MatrixXd Convertor_ToolBox::
vector2Eigen(const vector<vector<double > >  &objPts_in)
{
	Eigen::MatrixXd objPts_out = MatrixXd::Zero(objPts_in.size(),objPts_in[0].size());
	for(int i=0;i< objPts_in.size();i++)
	{
		for(int j=0;j<objPts_in[0].size();j++)
		{
			objPts_out(i,j) = objPts_in[i][j];
		}
	}
	return objPts_out;
}


#ifdef WITH_OPENCV
std::vector < cv::Rect > Convertor_ToolBox::
Eigen2RectVector_xywh ( const Eigen::MatrixXd mat_row_is_xywh )
{
    vector < cv::Rect > rois;
    for ( int i = 0; i < mat_row_is_xywh.rows(); i++ )
    {
        rois.push_back (cv::Rect( mat_row_is_xywh(i,0),mat_row_is_xywh(i,1),
                                  mat_row_is_xywh(i,2),mat_row_is_xywh(i,3) ) );

    }
    return rois;
}

#endif
