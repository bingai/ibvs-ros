#include "toolbox/Test_utilities.h"

using namespace std;
bool IsEqlMaxtrx (Eigen::MatrixXd A, Eigen::MatrixXd B, double tol)
{
	Eigen::MatrixXd test_results = A - B;

	if ( test_results.array().abs().maxCoeff() < tol)
		return true;
	else
		return false;
}



void print_TestResult (bool sign)
{

	if (sign)
	{
		std::cout << "####   Test....OK!   ####" <<std::endl;
	}
	else
	{
		std::cout << "####   Test....Failed!   ####"<<std::endl;
	}
}

bool calc_final_sign ( std::vector<bool>testResult, bool dispaly )
{
	bool final_sign = true;
	for (int i = 0; i < testResult.size(); i++)
	{
		final_sign = testResult[i]&final_sign;

	}

	if  (dispaly)
	{
		for (int i = 0; i < testResult.size(); i++)
		{
			cout << i<<":"<< testResult[i]<<"  , ";
		}
			 cout << endl;
			print_TestResult (final_sign);
	}

	return final_sign;
}


