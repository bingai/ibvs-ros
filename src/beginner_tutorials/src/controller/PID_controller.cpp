
#include "controller/PID_controller.h"

#define ONE_PAIR_JACOBIN_PROCESS 0
#define TWO_LINE_JACOBIN_PROCESS 1

using std::cout;
using std::endl;

/******************************************************
% dr = P_controller(Kp, ef, Jac_img_control,frame_rate)
% P_CONTROLLER
% A P controller to calculate the velocity of a point
%
% dr = P_controller(Kp, ef, Jac_img_control,frame_rate)
%
% Input:
%	Kp: 	Decay constant, larger = decay faster
%	ef: 	Error function
%	J_img_control:	Image jacobian for control
%	frame_rate:		Camera frame rate, Hz
%
% Output:
%	dr: 	6x1 velocity vector of a point
%	i.e. dr = [Vx; Vy; Vz; Wx; Wy; Wz]
%	where V are the translation velocity, W are angular velocity
%
% Copyright 2011-2013  PI Automation Department.
% $Revision: 1.1.0 	$Date: 17 Jan 2014
%
******************************************************/


Eigen::VectorXd PID_controller:: P_control(double Kp, const Eigen::VectorXd &ef, const Eigen::MatrixXd &J_img, double frame_rate)
{
    assert( ef.size() == J_img.rows() ); // length of error function nust be equal to no. of row of image jacobian

    double Pinv_Tol = 1e-6;
//cout << "J_img"<<endl<<J_img<<endl;

//#if ONE_PAIR_JACOBIN_PROCESS
 //   Eigen::MatrixXd J_img_3 = J_img.block (0,0,J_img.rows(),3);
//cout << "J_img_3"<<endl<<J_img_3<<endl;
//    Eigen::VectorXd pinvJef = pinv(J_img_3, Pinv_Tol)*ef;
#if TWO_LINE_JACOBIN_PROCESS
    Eigen::MatrixXd J_img_5 = J_img.block (0,0,J_img.rows(),5);
//cout << "J_img_5"<<endl<<J_img_5<<endl;
    Eigen::VectorXd pinvJef5 = pinv(J_img_5, Pinv_Tol)*ef;
    Eigen::VectorXd pinvJef(6);
    pinvJef << pinvJef5 , 0;
#else
    Eigen::VectorXd pinvJef = pinv(J_img, Pinv_Tol)*ef;
#endif


//cout << "pinvJef"<<endl<<pinvJef<<endl;


    Eigen::VectorXd dr =-Kp*pinvJef/frame_rate;
//cout << "dr"<<endl<<dr<<endl;
    return dr;
}



Eigen::MatrixXd PID_controller:: dr2cmd (  const Eigen::MatrixXd T_tr, const Eigen::MatrixXd dr )
{
    Eigen::MatrixXd PoseCmd(6,1);

    if (dr.size() == 3)
    {
cout << " Now, dr is 3*1"<<endl;

        T2Pose (T_tr,PoseCmd);

        for (int i = 0; i < 3; i++)
        {
            PoseCmd(i,0) = PoseCmd(i,0) +  dr(i,0);
        }
    }
    else
    {

        Eigen::MatrixXd Omega(3,1)  ,  V(3,1);

        Omega = dr.block<3,1>(3,0);
        V = dr.block<3,1>(0,0) ;

        Eigen::MatrixXd  sk_Omega = skew_matrix( Omega);

        Eigen::MatrixXd  T_rot= Eigen:: MatrixXd :: Identity(4,4);

        T_rot.block<3,3>(0,0) = T_rot.block<3,3>(0,0) + sk_Omega;

        T_rot.block<3,1>(0,3) = V;

        Eigen::MatrixXd  T_tr_new = T_rot * T_tr;

        T2Pose (T_tr_new,  PoseCmd);
    }

	return (PoseCmd);
}

/*
Eigen::MatrixXd PID_controller:: dr_obj2cmd (  const Eigen::MatrixXd T_tr, const Eigen::MatrixXd T_cr, const Eigen::MatrixXd T_oc_obj, const Eigen::MatrixXd dr_obj )
{
    VS_Kernel vsk;
    Eigen::MatrixXd T_or = T_cr*T_oc_obj;
    Eigen::MatrixXd dr = vsk.velocity_cam2robot(T_or)* dr_obj;

    return (dr2cmd(T_tr, dr));

}
*/





