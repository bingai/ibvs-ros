#include "controller/StopChecker.h"


using Eigen::MatrixXd;
//using Eigen::Vector2d;
using Eigen::VectorXd;
using std::cout;
using std::endl;

StopChecker::StopChecker(void){}

StopChecker::~StopChecker(void){}


void StopChecker::Init(const double dpixel_th,const double angle_th_deg)
{
    this->dpixel_th_ = dpixel_th;
    this->angle_th_deg_ = angle_th_deg;
}

/// check if object and target touch
bool StopChecker::IsTouch(const vsc_ImgFeatureSet &fset, const double dpixel_th)
{
    Eigen::MatrixXd dA, dB;

    dA = fset.cur_A-fset.tar_A;
    dB = fset.cur_B-fset.tar_B;

    if ((dA.norm() < dpixel_th) && (dB.norm() < dpixel_th)){
        std::cout << "They are touch!" <<  std::endl;
        return true;
    }
    else{
        std::cout << "They are not touch!" <<  std::endl;
        return false;
    }


}

/// Compute the angle between two lines
void StopChecker::compute_angle(const vsc_ImgFeatureSet &fset)
{

    Eigen::Vector2d vector_obj_A, vector_obj_B, vector_tar_A, vector_tar_B;
    std::cout << "compute angle" <<std::endl;
    vector_obj_A = fset.cur_A.col(0) - fset.cur_A.col(1);
    vector_obj_B = fset.cur_B.col(0) - fset.cur_B.col(1);
    vector_tar_A = fset.tar_A.col(0) - fset.tar_A.col(1);
    vector_tar_B = fset.tar_B.col(0) - fset.tar_B.col(1);

    this->cos_ang_A_ = std::abs(vector_obj_A.dot(vector_tar_A)/(vector_obj_A.norm()*vector_tar_A.norm()));
    this->cos_ang_B_ = std::abs(vector_obj_B.dot(vector_tar_B)/(vector_obj_B.norm()*vector_tar_B.norm()));

    //cout << "cos_ang_A_ = " << this->cos_ang_A_ << endl;
    //cout << "cos_ang_B_ = " << this->cos_ang_B_ << endl;

}


/// Check if two line are parallel
bool StopChecker::IsParallel(const vsc_ImgFeatureSet &fset, double angle_th_deg)
{
    assert(fset.cur_A.cols() == 2);
    VisGeo_PI vsg;
    double Th_ang_rad, cos_Th;

    this->compute_angle(fset);

    Th_ang_rad = vsg.deg2rad(angle_th_deg);
    cos_Th = cos(Th_ang_rad);

    if (this->cos_ang_A_ > cos_Th && this->cos_ang_B_ > cos_Th)
    {
        cout << "They are parallel!" <<endl;
        return true;
    }
    else
    {
        cout << "They are not parallel!" <<endl;
        return false;
    }
}




bool StopChecker::ShouldStop(const vsc_ImgFeatureSet &fset)

{
    //fset.print();
    assert(fset.tar_A.rows() == 2);
    //double LinNum = this->Compute_Line_num(fset);

    int num_cur_pt = fset.cur_A.cols();

    if (num_cur_pt == 1)
        return IsTouch(fset, this->dpixel_th_);
    else
        return IsParallel(fset, this->angle_th_deg_);


}






