/************************************************************
% Calc_image_jacobian
% compute the image jacobian for a camera
%
% J_img = Calc_image_jacobian(A, uk,vk, image_depth)
%
% Input:
%	A: 	3x3 Intrinsic matrix
%	uk:	1xN X-asix coordinate on image
%	vk:	1xN Y-asix coordinate on image
%	image_depth: 	1xN depth of image point
% 	N is number of points on image
%
% Output:
% 	J_img: 	2Nx6 Image jacobian matrix
%
% Copyright 2011-2 013  PI Automation Department.
% $Revision: 1.1.0 	$Date: 03 Dec 2013
%
  Modified:
  #2014-jan-15 MaLin:
  	add using namespace statement
  	changed function decalrations
*************************************************************/
//#include "librc.h"

#include <iostream>

#include "controller/VS_Kernel.h"

using namespace Eigen;
using namespace std;

/// Calculate the image Jacobian base on page 661 equation 47
Eigen::MatrixXd VS_Kernel::
Calc_image_jacobian
		(const Eigen::Matrix3d A, const Eigen::VectorXd uk, const Eigen::VectorXd vk, const Eigen::VectorXd image_depth)
{
int N; // Number of feature points
double fx, fy, cx, cy; // focal length and principal points
VectorXd z, u, v;
MatrixXd temp(2,6);


N = image_depth.size();	// get no. of point
MatrixXd J_img(2*N,6);	// matrix to store result

// load the intrinsic parameters
fx = A(0,0);
fy = A(1,1);
cx = A(0,2);
cy = A(1,2);

// load the depth of image points
z = image_depth;

// move the image point to centre
u = uk-MatrixXd::Constant(N,1,cx);
v = vk-MatrixXd::Constant(N,1,cy);

// Computet the image jocobian
for (int k=0; k<N; k++)
{
	temp <<  fx/z(k),    0,       -u(k)/z(k),    -( u(k)*v(k) ) / fy,    	     (fx*fx+u(k)*u(k))/fx,    -(fx*v(k))/fy,
					0,         fy/z(k),  -v(k)/z(k),    -(fy*fy+v(k) * v(k) )/fy,    u(k)*v(k)/fx,                     fy*u(k)/fx;

	J_img.block<2,6>(2*k,0) << temp;
}

return J_img;
}
/**********************************************************************
% MATCH_PL
% A point to line match function
%
% [ef_pl, J_img_pl] = match_pl(imgxy_objpt_a, imgxy_objpt_b, imgxy_tarpt_a, imgxy_tarpt_b, imgz_objpt_a, imgz_objpt_b, T_cr_a, T_cr_b, A_cama, A_camb)
%
% This function compute the error function and image jacobian for point to point matching
%
% Input:
%	imgxy_objpt_a: 	2xN image feature point matrix of object in camera A
%	imgxy_objpt_b: 	2xN image feature point matrix of object in camera B
%	imgxy_tarpt_a: 	2xN image feature point matrix of target in camera A
%	imgxy_tarpt_b: 	2xN image feature point matrix of target in camera B
%	imgz_objpt_a: 	1xN depth vector of object in camera A
%	imgz_objpt_b: 	1xN depth vector of object in camera B
%	T_cr_a: 		4x4 Transform matrix from camera A to robot base coordinate
%	T_cr_b: 		4x4 Transform matrix from camera B to robot base coordinate
%	A_cama: 		3x3 Intrinsic matrix of camera A
% 	A_camb: 		3x3 Intrinsic matrix of camera B
%
% 	i.e. imgxy_xxx_x =   	[	u1	u2 	...	uN 	]	<-- camera x
%			[ 	v1 	v2 	... 	vN  	]	<-- camera x
%
% Output:
%	ef_pl: 		Error function
%	J_img_pl: 		Image jacobian of point to line matching
%
% Copyright 2011-2013  PI Automation Department.
% $Revision: 1.1.0 	$Date: 15 Jan 2014
************************************************************************/

/// Calculate the image Jacobian base on page 663 equation 57
Eigen::MatrixXd
VS_Kernel:: match_pl
		(const Eigen::MatrixXd imgxy_objpt_a, const Eigen::MatrixXd imgxy_objpt_b,
		const Eigen::MatrixXd imgxy_tarpt_a, const Eigen::MatrixXd imgxy_tarpt_b,
		const Eigen::RowVectorXd imgz_objpt_a, const Eigen::RowVectorXd imgz_objpt_b,
		const Eigen::Matrix4d T_cr_a, const Eigen::Matrix4d T_cr_b, const Eigen::Matrix3d A_cama, const Eigen::Matrix3d A_camb)
{

// Check the no. of point of target
int num_tarpt = imgxy_tarpt_a.cols();
int num_objpt = imgxy_objpt_a.cols();

assert(num_tarpt == 2);

// define variable
Eigen::MatrixXd    velocity_matrix_ca2r, velocity_matrix_cb2r;
Eigen::MatrixXd    J_img_a, J_img_b;
Eigen::MatrixXd    J_img_pl_a(num_objpt,6), J_img_pl_b(num_objpt,6), J_img_pl(2*num_objpt,6);

// compute velocity screw from camera to robot base
velocity_matrix_ca2r= velocity_cam2robot(T_cr_a);
velocity_matrix_cb2r= velocity_cam2robot(T_cr_b);

// compute the error function for point line matching
Eigen::MatrixXd    imgxy_tarpt_a_with_depth(3,num_tarpt), imgxy_tarpt_b_with_depth(3,num_tarpt);
Eigen::MatrixXd    imgxy_objpt_a_with_depth(3,num_objpt), imgxy_objpt_b_with_depth(3,num_objpt);
Eigen::Vector3d    tarpt_a_cross, tarpt_b_cross;

// Append a constant at the 3rd row of all point vectors
imgxy_tarpt_a_with_depth << imgxy_tarpt_a, Eigen::MatrixXd::Ones(1,2)*0.01;
imgxy_tarpt_b_with_depth << imgxy_tarpt_b, Eigen::MatrixXd::Ones(1,2)*0.01;
imgxy_objpt_a_with_depth << imgxy_objpt_a, Eigen::MatrixXd::Ones(1,num_objpt)*0.01;
imgxy_objpt_b_with_depth << imgxy_objpt_b, Eigen::MatrixXd::Ones(1,num_objpt)*0.01;

// Calculatet the cross product of target pts
Eigen::Vector3d Vec1_temp, Vec2_temp;
Vec1_temp = imgxy_tarpt_a_with_depth.col(0);
Vec2_temp = imgxy_tarpt_a_with_depth.col(1);
tarpt_a_cross = Vec1_temp.cross(Vec2_temp);

Vec1_temp = imgxy_tarpt_b_with_depth.col(0);
Vec2_temp = imgxy_tarpt_b_with_depth.col(1);
tarpt_b_cross = Vec1_temp.cross(Vec2_temp);

// Compute the error function
Eigen::RowVectorXd ef_pl_a, ef_pl_b;
Eigen::VectorXd ef_pl(2*num_objpt);
ef_pl_a = tarpt_a_cross.transpose()*imgxy_objpt_a_with_depth;
ef_pl_b = tarpt_b_cross.transpose()*imgxy_objpt_b_with_depth;
ef_pl << ef_pl_a.transpose(), ef_pl_b.transpose();


// compute image jacobian for each camera
Eigen::MatrixXd J_temp(3,6);
J_img_a = Calc_image_jacobian(A_cama, imgxy_objpt_a.row(0), imgxy_objpt_a.row(1), imgz_objpt_a)*velocity_matrix_ca2r.inverse();
J_img_b = Calc_image_jacobian(A_camb, imgxy_objpt_b.row(0), imgxy_objpt_b.row(1), imgz_objpt_b)*velocity_matrix_cb2r.inverse();

for (int kk=0; kk < num_objpt; kk++){
J_temp << J_img_a.block<2,6>(2*kk,0), Eigen::MatrixXd::Zero(1,6);
J_img_pl_a.row(kk) << tarpt_a_cross.transpose()*J_temp;

J_temp << J_img_b.block<2,6>(2*kk,0), Eigen::MatrixXd::Zero(1,6);
J_img_pl_b.row(kk) << tarpt_b_cross.transpose()*J_temp;
}

J_img_pl << J_img_pl_a, J_img_pl_b;

// Return for use
Eigen::MatrixXd J_ef(2*num_objpt,7);

J_ef << J_img_pl, ef_pl;

return J_ef;
}

/**************************************************************
% MATCH_PP
% A point to point match function
%
% [ef_pp, J_img_pp] = match_pp(imgxy_objpt_a, imgxy_objpt_b, imgxy_tarpt_a, imgxy_tarpt_b, imgz_objpt_a, imgz_objpt_b, T_cr_a, T_cr_b, A_cama, A_camb)
%
% This function compute the error function and image jacobian for point to point matching
%
% Input:
%	imgxy_objpt_a: 	2xN image feature point matrix of object in camera A
%	imgxy_objpt_b: 	2xN image feature point matrix of object in camera B
%	imgxy_tarpt_a: 	2xN image feature point matrix of target in camera A
%	imgxy_tarpt_b: 	2xN image feature point matrix of target in camera B
%	imgz_objpt_a: 	1xN depth vector of object in camera A
%	imgz_objpt_b: 	1xN depth vector of object in camera B
%	T_cr_a: 		4x4 Transform matrix from camera A to robot base coordinate
%	T_cr_b: 		4x4 Transform matrix from camera B to robot base coordinate
%	A_cama: 		3x3 Intrinsic matrix of camera A
% 	A_camb: 		3x3 Intrinsic matrix of camera B
%
% 	i.e. imgxy_xxx_x =   	[	u1	u2 	...	uN 	]	<-- camera x
%			[ 	v1 	v2 	... 	vN  	]	<-- camera x
%
% Intermediate output:
%	ef_pp: 		4Nx1 Error function 	f_obj - f_tar
%	J_img_pp: 	4Nx6 Image jacobian of point to point matching
% Output:
%	J_ef: 		4Nx7 [J_img_pp, ef_pp]
%
%	i.e. 		J_img_pp = J_ef.block<4N,6>(0,0);
%			ef_pp = J_ef.col(6);
%
% Copyright 2011-2013  PI Automation Department.
% $Revision: 1.1.0 	$Date: 03 Dec 2013
%
*********************************************************/

/// Calculate the image Jacobian base on page 661 equation 48
Eigen::MatrixXd
VS_Kernel :: match_pp
		(const Eigen::MatrixXd &imgxy_objpt_a, const Eigen::MatrixXd &imgxy_objpt_b, const Eigen::MatrixXd &imgxy_tarpt_a, const Eigen::MatrixXd &imgxy_tarpt_b,
            const Eigen::RowVectorXd &imgz_objpt_a, const Eigen::RowVectorXd &imgz_objpt_b, const Eigen::Matrix4d &T_cr_a, const Eigen::Matrix4d &T_cr_b, const Eigen::Matrix3d &A_cama, const Eigen::Matrix3d &A_camb){

// define variable
int Ja_rows, Jb_rows;
Eigen::MatrixXd velocity_matrix_ca2r, velocity_matrix_cb2r;
Eigen::MatrixXd J_img_a, J_img_b;

// compute velocity screw from camera to robot base
velocity_matrix_ca2r= velocity_cam2robot(T_cr_a);
velocity_matrix_cb2r= velocity_cam2robot(T_cr_b);

// compute image jacobian for each camera
J_img_a = Calc_image_jacobian(A_cama, imgxy_objpt_a.row(0),imgxy_objpt_a.row(1), imgz_objpt_a)
                *velocity_matrix_ca2r.inverse();
J_img_b = Calc_image_jacobian(A_camb, imgxy_objpt_b.row(0),imgxy_objpt_b.row(1), imgz_objpt_b)
                *velocity_matrix_cb2r.inverse();

// Combine the image jacobian of two cameras
Ja_rows = J_img_a.rows();
Jb_rows = J_img_b.rows();
Eigen::MatrixXd J_img_pp(Ja_rows+Jb_rows, 6);
J_img_pp << J_img_a, J_img_b;

// Arrange the feature vectors
Eigen::VectorXd f_obj(Ja_rows+Jb_rows), f_tar(Ja_rows+Jb_rows), ef_pp(Ja_rows+Jb_rows);
Eigen::MatrixXd temp_imgxy_1, temp_imgxy_2;
temp_imgxy_1 = imgxy_objpt_a;
temp_imgxy_1.resize(Ja_rows,1);

temp_imgxy_2 = imgxy_objpt_b;
temp_imgxy_2.resize(Jb_rows,1);
f_obj << temp_imgxy_1, temp_imgxy_2;

temp_imgxy_1 = imgxy_tarpt_a;
temp_imgxy_1.resize(Ja_rows,1);
temp_imgxy_2 = imgxy_tarpt_b;
temp_imgxy_2.resize(Jb_rows,1);
f_tar <<temp_imgxy_1, temp_imgxy_2;

//equation 56, at paper
ef_pp = f_obj-f_tar;

Eigen::MatrixXd J_ef(Ja_rows+Jb_rows,7);

J_ef << J_img_pp, ef_pp;
return J_ef;
}

/// Compute the velocity screw from camera coordinate to robot coordinate base on page 653 section B of paper
Eigen::MatrixXd VS_Kernel ::
  velocity_cam2robot(const Eigen::Matrix4d &T_cr)
{

Eigen::Matrix3d sk_t;
Eigen::MatrixXd VM_c2r(6,6);

Eigen::Matrix3d R_cr = T_cr.block<3,3>(0,0);
Eigen::Vector3d t_cr = T_cr.block<3,1>(0,3);

sk_t = skew_matrix(t_cr);


VM_c2r << R_cr, 	sk_t*R_cr,
	  Eigen::MatrixXd::Zero(3,3), R_cr;
return VM_c2r;

/// Modified version Uncommet to use it
/*Eigen::Matrix3d sk_t, cof;
Eigen::MatrixXd VM_c2r(6,6);


Eigen::Matrix3d R_cr = T_cr.block<3,3>(0,0);
Eigen::Vector3d t_cr = T_cr.block<3,1>(0,3);

sk_t = skew_matrix(R_cr.transpose()*t_cr);

cof = matrix_cofactor(R_cr);

VM_c2r << R_cr, 	R_cr*sk_t,
	  Eigen::MatrixXd::Zero(3,3), cof;

return VM_c2r;
*/
}



void VS_Kernel::
Extract_Jimg_ef ( const Eigen::MatrixXd & Jimg_ef,  Eigen::VectorXd &ef,  Eigen::MatrixXd &J_img)
{
    int END  = Jimg_ef.cols();
	int row_num = Jimg_ef.rows();
	Eigen::VectorXd   ef__ = Jimg_ef.col (END-1);
	Eigen::MatrixXd   J_img__ = Jimg_ef.block ( 0,0,row_num, END-1 );
    ef =  ef__;
    J_img = J_img__;
}





