#include <iostream>

#include "controller/DepthPredictor.h"

using  Eigen::MatrixXd;


double DepthPredictor::
grab_SingleElemMat (Eigen::MatrixXd A)
{
	assert (  (  A.rows() == 1) && ( A.cols() == 1)  );
	return (A(0,0));
}

bool DepthPredictor::
gen_LineSeg_once ( const Eigen::MatrixXd Lines_byCol, Eigen::MatrixXd &  LineSegPoints_byCol , int Point2Extrin_unit_ExchangeRate)
{
	int line_num = Lines_byCol.cols();
	int line_dim = Lines_byCol.rows();

	assert ( (  line_num == 1 ) && (  line_dim == 3 )  );

	double norm_inv = 1 /  Lines_byCol.norm();
	double scale;

	Eigen::MatrixXd EndPoints = MatrixXd::Zero( 3,2 );


	scale = 0.1 * norm_inv * Point2Extrin_unit_ExchangeRate;
	EndPoints.col(0) = Lines_byCol  * scale;
	scale = 100 * scale;
	EndPoints.col(1) = Lines_byCol  * scale;

	LineSegPoints_byCol =EndPoints;
	return true;
}



double DepthPredictor::
detect_Dist_btw_2Lines( const Eigen::MatrixXd &  LineSegPoints_1, const Eigen::MatrixXd &  LineSegPoints_2,
        Eigen::MatrixXd & NearestPts,    Eigen::MatrixXd & virtual_sect_point )
{
	assert(  (  LineSegPoints_1.rows() == 3  )  && (  LineSegPoints_1.cols() == 2 )  );
	assert(  (  LineSegPoints_2.rows() == 3  )  && (  LineSegPoints_2.cols() == 2)  );

	Eigen::MatrixXd LineSegPoints_1_t = LineSegPoints_1.transpose();
	Eigen::MatrixXd LineSegPoints_2_t = LineSegPoints_2.transpose();
	Eigen::MatrixXd P0 = LineSegPoints_1_t.row(0);
	Eigen::MatrixXd P1 = LineSegPoints_1_t.row(1);
	Eigen::MatrixXd Q0 = LineSegPoints_2_t.row(0);
	Eigen::MatrixXd Q1 = LineSegPoints_2_t.row(1);

	Eigen::MatrixXd  u = P1-P0;
	Eigen::MatrixXd  v =Q1-Q0;
	Eigen::MatrixXd  w0 = P0 - Q0;
//abs(u*v')==norm(u)*norm(v)
	Eigen::MatrixXd b_mat = u * v.transpose();
	double b = grab_SingleElemMat ( b_mat );

    assert ( std::abs(b) != ( u.norm() * v.norm()  ) );

	Eigen::MatrixXd    a_mat=u * u.transpose();
	Eigen::MatrixXd    c_mat=v * v.transpose();
	Eigen::MatrixXd    d_mat=u * w0.transpose();
	Eigen::MatrixXd   e_mat =v * w0.transpose();
	double a = grab_SingleElemMat ( a_mat );
	double c = grab_SingleElemMat ( c_mat );
	double d = grab_SingleElemMat ( d_mat );
	double e = grab_SingleElemMat ( e_mat );

	double part_result =  1 /   ( a * c - std::pow( b,2 ) );
	double 	sc =( b * e - c * d ) * part_result;
	double 	tc = ( a * e - b * d ) * part_result;



	Eigen::MatrixXd dist_mat =  ( w0 + ( sc * u -tc * v)  );;
	double dist =dist_mat.norm();
	Eigen::MatrixXd    Pc=P0 +sc*u;
	Eigen::MatrixXd    Qc=Q0+tc*v;

	Eigen::MatrixXd	mid_pt = (Pc + Qc) * 0.5;

	Eigen::MatrixXd NearestPts_byCol = Eigen::MatrixXd::Zero(3,2);
	NearestPts_byCol.col( 0 ) = Pc.transpose();
	NearestPts_byCol.col( 1 ) = Qc.transpose();

	NearestPts = NearestPts_byCol;
	virtual_sect_point = mid_pt.transpose();

	return dist;
}




Eigen::MatrixXd  DepthPredictor::
Esti_Depth ( const Eigen::Matrix3d inv_Intrinsic_A, const Eigen::Matrix3d inv_Intrinsic_B, const Eigen::MatrixXd Img_Pts_A, const Eigen::MatrixXd Img_Pts_B,
								  								const Eigen::MatrixXd T_camA2camB,  Eigen::MatrixXd & interSect_Pts_A,   Eigen::MatrixXd & interSect_Pts_B )
 {
 		assert (  (  Img_Pts_A.rows()  == 2  ) && (  Img_Pts_B.rows()  == 2  )  );
 		assert (  Img_Pts_A.cols()  == Img_Pts_B.cols()  );

		int point_num = Img_Pts_A.cols();
		//data decalaration
		Eigen::MatrixXd    Project_Lines_A, Project_Lines_B ;
        Eigen::MatrixXd   dist_row = MatrixXd::Zero( 1 , point_num);
		Eigen::MatrixXd    LineSegPoints_byCol,  LineSegPoints_AatB, interSect_Point,   LineSegPoints_AatB_homo;
		Eigen::MatrixXd    interSect_Point_byCol ( 3, point_num),  interSect_Point_byCol_camA ( 3, point_num);
		Eigen::MatrixXd    LineSegPoints_A,  LineSegPoints_B,  NearestPts,  LineSegPoints_A_homo;

        vg.BackProject(   inv_Intrinsic_A,Img_Pts_A,  Project_Lines_A  );
        vg.BackProject(   inv_Intrinsic_B,Img_Pts_B,  Project_Lines_B  );

		// TOUP:  can be witten as  process once+loop+container mode
        for ( int i =0; i < point_num; i++)
		{

			gen_LineSeg_once  ( Project_Lines_A.col(i),   LineSegPoints_A ,1000 );
			gen_LineSeg_once  ( Project_Lines_B.col(i),   LineSegPoints_B ,1000 );


			vg.TransformPoints( T_camA2camB,  LineSegPoints_A ,  LineSegPoints_AatB );
			dist_row(0,i) = detect_Dist_btw_2Lines (  LineSegPoints_AatB, LineSegPoints_B, NearestPts, interSect_Point);
			interSect_Point_byCol.col(i) = interSect_Point;
		}

		vg.TransformPoints ( T_camA2camB.inverse(),  interSect_Point_byCol,    interSect_Point_byCol_camA);
		interSect_Pts_B = interSect_Point_byCol;
		interSect_Pts_A = interSect_Point_byCol_camA;


		return dist_row;

 }

void DepthPredictor::
Esti_3d ( vsc_ImgFeatureSet & Points3d_Set, const vsc_ImgFeatureSet featureSet, const vsc_EnvironVar env_var )
{
    Eigen::MatrixXd objA3, objB3, tarA3,tarB3; // these are 3d points buffer
    Esti_Depth ( env_var.inv_intrinsic_A, env_var.inv_intrinsic_B, featureSet.cur_A,featureSet.cur_B,
									env_var.T_cA2cB,  objA3, objB3 ) ;
	Esti_Depth ( env_var.inv_intrinsic_A, env_var.inv_intrinsic_B, featureSet.tar_A,featureSet.tar_B,
									env_var.T_cA2cB,  tarA3, tarB3 ) ;
    Points3d_Set.set(tarA3,tarB3,objA3, objB3);

}

