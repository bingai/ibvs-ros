
#include "controller/VisualServoingController_PI.h"

/** implement class: VisualServoingController_PI **/

// constructor


/**
	Eigen::MatrixXd  Run ( const vsc_ImgFeatureSet  featureSet, const Eigen::MatrixXd Robot_ctrl_vector_now )
**/

Eigen::MatrixXd  VisualServoingController_PI::
Run ( const vsc_ImgFeatureSet  featureSet, const Eigen::MatrixXd Robot_ctrl_vector_now )
{
        assert (Finished_Init == true);
		assert ( this->Is_LoadData_EnvironVar == true );

		int VSmode = featureSet.tar_A.cols(); //VSmode can is eqaul to the no. of point of target

		Eigen::MatrixXd   T_tr;     // Transformation matrix from tool coordinate to robot coordinate
		Eigen::MatrixXd   estiPoints_atA, estiPoints_atB;

	 	vp.Pose2T (Robot_ctrl_vector_now,T_tr );
		sysData.inv_intrinsic_A =sysData.Intrinsc_A.inverse( );
		sysData.inv_intrinsic_B =sysData.Intrinsc_B.inverse( );

        /// Estimate 3D point in cmaera coordinate of object from the image feature set
	 	dp.Esti_Depth (   featureSet,  this->sysData , estiPoints_atA, estiPoints_atB  );

        ///extract the depth of the object in camera coordinate
		Eigen::MatrixXd  depth_A = estiPoints_atA.row(2);
		Eigen::MatrixXd  depth_B = estiPoints_atB.row(2);

        /// compute the image Jacobian and error vector based on page 661-663 of the paper
        Eigen::MatrixXd  Jimg_ef;
        if (VSmode == 1)
        /// point to point visual servoing
		Jimg_ef = vsk.match_pp  ( featureSet,  depth_A, depth_B ,  this->sysData );
		else if (VSmode == 2)
		/// line to line visual servoing
		Jimg_ef = vsk.match_pl  ( featureSet,  depth_A, depth_B ,  this->sysData );
		else
		{
		std::cout << "no. of points is not correct!" << std::endl;
		return Eigen::MatrixXd::Zero(6,1);
		}

        ///Extract the error vector ef, and the image Jacobian from the Jimg_ef
		Eigen::MatrixXd  ef = Jimg_ef.col (Jimg_ef.cols()-1);
		Eigen::MatrixXd  Jacob_Img = Jimg_ef.leftCols (6);
		///Use P control to compute the control input dr
        Eigen::MatrixXd  dr = p_ctrl.P_control( Kp, ef, Jacob_Img,  frame_rate);
        /// Convert the control input dr to 6 axis robot control command
		Eigen::MatrixXd Robot_ctrl_vector_next = p_ctrl.dr2cmd (   T_tr,  dr );
		//std::cout <<"Robot_ctrl_vector_next" <<std::endl<<Robot_ctrl_vector_next<<std::endl;

		return ( Robot_ctrl_vector_next );
}// Run



int VisualServoingController_PI::
init( const	int Kp_ ,const int frame_rate_ )
{
    this-> Kp = Kp_ ;
	this-> frame_rate = frame_rate_;
	Finished_Init = true;
}

Eigen::MatrixXd  VisualServoingController_PI::
Run ( const vsc_ImgFeatureSet  featureSet, const Eigen::MatrixXd Robot_ctrl_vector_now );



bool VisualServoingController_PI::
LoadData_EnvironVar ( const std::string dataset_Path)
{
/*
Environment variable including:
Intrinsic_X: Intrinsic Matrix of pine-hole camera model for both cameras
T_cr_X: Transformation matrix from camera coordinate to robot coordinate
T_cA2cB: transformation matrix from camera A coordinate to camera B coordinate
*/
		sysData.Intrinsc_A = read_txt(dataset_Path+ "A_cama" + ".txt");
		sysData.Intrinsc_B = read_txt(dataset_Path+ "A_camb" + ".txt");
		sysData.T_cr_A = read_txt(dataset_Path+ "T_cr_a" + ".txt");
		sysData.T_cr_B = read_txt(dataset_Path+ "T_cr_b" + ".txt");
		sysData.T_cA2cB = read_txt(dataset_Path+ "Rt_cama2camb" + ".txt");

		sysData. inv_intrinsic_A  =  sysData.Intrinsc_A. inverse();
		sysData. inv_intrinsic_B  =  sysData.Intrinsc_B. inverse();

		Is_LoadData_EnvironVar = true;
		return true;
}


