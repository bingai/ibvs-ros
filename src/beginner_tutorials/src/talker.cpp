#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>

#include "controller/VisualServoingController_PI.h"
#include "simulator/Simluator_Simple.h"
#include "toolbox/IO_ToolBox_Eigen.h"
#include "vsc_DataStructure.h"

#include <string>
#include <iostream>

#include "Eigen/Dense"


using std::string;
using Eigen::MatrixXd;

int main(int argc, char **argv)
{
/// Choose the VS mode, 1= point to point visual servoing
/// 2 for line to line visual servoing
int VSmode = 1;

Simulator_Simple sim; // define the simulator
VisualServoingController_PI controller; // define controller
IO_ToolBox_Eigen iot;   // define io passer

/***********************************************************
// image feature set is the input the visual servoing
// It contain the pixel values of moving object and the
// target we need to reach, in the format of
// pin1_pt1     pin1_pt2    pin2_pt1    pin2_pt2    ...    pointn
// x1           x2                                  ...    xn
// y1           y2                                  ...    yn
************************************************************/
vsc_ImgFeatureSet fset; // define the image feature set

/// Set the variable directory
string SimData_path = "./Simulator_dataSet/";
string ControllerData_path = "./Controller_dataSet/";

/// Define the P controller gain
const int Kp = 1;
const int FrameRate = 15;
bool stop = false;

/// read the current position of robot
MatrixXd robot_now = iot.read_txt(SimData_path+"control_vec.txt");


/// init simulator
sim.Init_ReWorld( SimData_path, VSmode);

/// init Stoper
double dpixels = 2;
double ang_th_deg = 3;
controller.stopper.Init(dpixels, ang_th_deg);

/// Load the environment setting
controller.LoadData_EnvironVar( ControllerData_path );
controller.init(Kp,FrameRate);

/// Compute the feature set in simulator
sim.Renew_World( robot_now,  fset,   controller.sysData );

/// Output the initial feature set
std::cout << "Inputed feature set = " << std::endl;
fset.print();

/// Pause to see the visual servoing mode
std::cout << "Press Enter to continue!";
std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');

/// start visual servoing
int i = 0;
while (!stop)
{
    ///Stop if reached our goal
    stop = controller.stopper.ShouldStop(fset);
    /**************************************
    // Visual servoing CORE function
    // input: image feature set and current robot position
    // output: next robot position
    ***************************************/
    MatrixXd robot_next = controller.Run(fset, robot_now);
    robot_now = robot_next;

    
    ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    //////////////////////////////////ROS Publisher////////////////////
    
    ros::init(argc, argv, "talker");
    ros::NodeHandle n;
    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
    ros::Rate loop_rate(10);
    
    
    int count = 0;
    while (ros::ok())
  {
    std_msgs::String msg;

    std::stringstream ss;
    ss = robot_now;
    msg.data = ss.str();

    ROS_INFO("%s", msg.data.c_str());

    chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }

    ////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////

  

    /// Detect the image feature of object and target again after each move
    sim.Renew_World( robot_now,  fset,   controller.sysData );

    /// Uncomment the following line to see the intermediate result
    //std::cout << "next robot command after " << i << " iteration = " << std::endl << robot_next << std::endl;
    //std::cout << "Intermediate feature set after "<< i << " iteration = " << std::endl;
    //fset.print();

    i++;
}
/// Dispaly the result
std::cout << "Finish at " << i << " iteration!" << std::endl;
std::cout << "Final feature set = " << std::endl;
fset.print();

    return 1;
}
