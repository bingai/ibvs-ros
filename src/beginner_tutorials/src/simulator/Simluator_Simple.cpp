
#include "simulator/Simluator_Simple.h"


	/*******************
	The parameters init_ed in this function are const.
	*******************/
	void Simulator_Simple:: Init_ReWorld
            (   const Eigen::MatrixXd &objpt_t__,
				const Eigen::MatrixXd &tarpt_r__,
				const Eigen::MatrixXd &cama_pos__,
				const Eigen::MatrixXd &camb_pos__,
				const Eigen::Matrix3d &A_cama__,
				const Eigen::Matrix3d &A_camb__ )
	{
				 this-> objpt_t = objpt_t__;
				 this-> tarpt_r=tarpt_r__;
				 this-> cama_pos=cama_pos__;
				 this-> camb_pos=camb_pos__;
				 this-> A_cama=A_cama__;
				 this-> A_camb=A_camb__;
	}

    void Simulator_Simple::Init_ReWorld ( std::string DataSet_Path, const int VSmode)
    {
		 this-> A_cama = read_txt(DataSet_Path+ "A_cama" + ".txt");
		 this-> A_camb = read_txt(DataSet_Path+ "A_camb" + ".txt");
		 this-> cama_pos = read_txt(DataSet_Path+ "cama_pos" + ".txt");
		 this-> camb_pos = read_txt(DataSet_Path+ "camb_pos" + ".txt");


        ///Load input feature set according to visual servoing mode

         if (VSmode == 1)
		 {
            this-> objpt_t = read_txt(DataSet_Path+ "objpt_t" + ".txt");
            this-> tarpt_r = read_txt(DataSet_Path+ "tarpt_r" + ".txt");
            std::cout << "You are running point to point visusl servoing!" << std::endl;
		 }
		 else if (VSmode ==2)
		 {
            this-> objpt_t = read_txt(DataSet_Path+ "objpt_t2" + ".txt");
            this-> tarpt_r = read_txt(DataSet_Path+ "tarpt_r2" + ".txt");
            std::cout << "You are running line to line visusl servoing!" << std::endl;
		 }
         else
         {
             std::cout << "Your VSmode are not valid, this simple program can only handle the point to point and line to line operation!" << std::endl;
             std::cout << "VSmode = 1 for point to point!"<< std::endl;
             std::cout << "VSmode = 2 for line to line!"<< std::endl;

         }


   }




	/**************************
	When robot moves, where is the feature points on images.
	Input: Renew_world.
	output, new feature image coordinates. new Ttr
	*****************************/
	void Simulator_Simple::Renew_World
	 (const Eigen::MatrixXd & control_vec,  vsc_ImgFeatureSet & featureSet,   vsc_EnvironVar & env_var )
	{
//			std::cout << "   control_vec  " <<std::endl << control_vec <<std::endl;
//			featureSet.print();

		Eigen::MatrixXd tarpt_ca, tarpt_cb;

		Pose2T( this->cama_pos,  env_var.T_cr_A);
		Pose2T( this->camb_pos,  env_var.T_cr_B);
//	std::cout << "   env_var.T_cr_A  " <<std::endl << env_var.T_cr_A <<std::endl;
//	std::cout << "   env_var.T_cr_A  " <<std::endl << env_var.T_cr_B <<std::endl;
		Eigen::MatrixXd T_rc_A = env_var.T_cr_A.inverse();
		Eigen::MatrixXd T_rc_B = env_var.T_cr_B.inverse();
/////////////////////////////////////////////////////////////////////////
		TransformPoints( T_rc_A,  this->tarpt_r,  tarpt_ca);
		TransformPoints( T_rc_B,  this->tarpt_r,  tarpt_cb);
//	std::cout << "   tarpt_ca  " <<std::endl <<tarpt_ca <<std::endl;
//	std::cout << "   tarpt_cb " <<std::endl <<tarpt_cb <<std::endl;
		ProjectToImagePlane( A_cama,    tarpt_ca,  featureSet.tar_A );
		ProjectToImagePlane( A_camb,   tarpt_cb,  featureSet.tar_B);
//	std::cout << "    featureSet.tar_A  " <<std::endl << featureSet.tar_A <<std::endl;
//	std::cout << "    featureSet.tar_B " <<std::endl << featureSet.tar_B <<std::endl;
///////////////////////////////////////////////////////////////////////////
        Pose2T (control_vec, this-> T_tr );
		TransformPoints (this-> T_tr, this->objpt_t, this->objpt_r );
//	std::cout << "   T_tr  " <<std::endl << T_tr <<std::endl;
//	std::cout << "    objpt_r " <<std::endl << objpt_r <<std::endl;
		TransformPoints(T_rc_A,  this->objpt_r, this->objpt_ca );
		TransformPoints(T_rc_B,  this->objpt_r, this->objpt_cb );
//	std::cout << "   this->objpt_ca  " <<std::endl << this->objpt_ca <<std::endl;
//	std::cout << "    this->objpt_cb " <<std::endl << this->objpt_cb <<std::endl;
		ProjectToImagePlane( A_cama,    this->objpt_ca,  featureSet.cur_A );
		ProjectToImagePlane( A_camb,   this->objpt_cb,  featureSet.cur_B);
//	std::cout << "    featureSet.cur_A  " <<std::endl <<  featureSet.cur_A <<std::endl;
//	std::cout << "     featureSet.cur_B " <<std::endl <<  featureSet.cur_B <<std::endl;
///////////////////////////////////////////
		env_var.T_cA2cB = env_var.T_cr_B.inverse() * env_var.T_cr_A;
//	std::cout << "     T_cA2cB " <<std::endl <<  env_var.T_cA2cB <<std::endl;
		env_var.Intrinsc_A = this->A_cama;
		env_var.Intrinsc_B = this->A_camb;

//		env_var.print();

	}



