#pragma once

// focused lib

//C,C++
#include <string>
#include <iostream>
// Other dependency
#include "toolbox/Math_ToolBox.h"
#include "toolbox/IO_ToolBox_Eigen.h"
#include "toolbox/VisGeo.h"

// In-project header
#include "controller/VS_Kernel.h"
#include "controller/DepthPredictor.h"
#include "controller/PID_controller.h"
#include "controller/StopChecker.h"


#include "vsc_DataStructure.h"




class VisualServoingController_PI:
    public IO_ToolBox_Eigen, public Math_ToolBox
{
public:
	VisualServoingController_PI()
	{
		Is_LoadData_EnvironVar = false;
		Finished_Init = false;
	}
	~VisualServoingController_PI(){}
//------------------------------------------//
//      data members                        //
//------------------------------------------//
    StopChecker stopper;
    VisGeo_PI vp;
    VS_Kernel vsk;
    DepthPredictor dp;
    PID_controller p_ctrl;
    vsc_EnvironVar  sysData;
//------------------------------------------//
//      method members                        //
//------------------------------------------//
    int init( const	int Kp_ ,const int frame_rate_ );

	Eigen::MatrixXd  Run ( const vsc_ImgFeatureSet  featureSet, const Eigen::MatrixXd Robot_ctrl_vector_now );

	bool  LoadData_EnvironVar ( const std::string dataset_Path);

	inline bool  Is_LoadData()	 { return Is_LoadData_EnvironVar;}


private:
    // flags
	bool Is_LoadData_EnvironVar;
	bool Finished_Init;
    //param
    int Kp ;
	int frame_rate;

};
