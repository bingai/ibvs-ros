#ifndef __STOPCHECKER__H_
#define __STOPCHECKER__H_

#include <iostream>
#include <assert.h>
#include "Eigen/Eigen"
#include "Eigen/Dense"
#include "vsc_DataStructure.h"
#include "toolbox/VisGeo.h"


//struct vsc_ImgFeatureSet;
//class VisGeo_PI

class StopChecker
{
public:

    StopChecker(void);
   //    StopChecker(const StopChecker_Dat & InputDataSet );
    ~StopChecker(void);

    void Init(const double dpixel_th,const double angle_th_deg);

    /// true if touch for point to point visual servoing
    /// true if object line and target line are parallel for line to line visual servoing
    bool ShouldStop(const vsc_ImgFeatureSet &fset);

private:
    /// check if two lines are parallel
    bool IsParallel(const vsc_ImgFeatureSet &fset, double angle_th_deg);

    /// check if object and target touch each other
    bool IsTouch(const vsc_ImgFeatureSet &fset, const double dpixel_th);

    /// find the angle between two lines
    void compute_angle(const vsc_ImgFeatureSet &fset);

private:

    double angle_th_deg_;
    double dpixel_th_;
    double cos_ang_A_, cos_ang_B_;


};



#endif
