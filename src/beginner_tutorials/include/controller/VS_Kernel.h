
/***************************************
declararion of image jacobin calculator.

***************************************/
#ifndef _VS_Kernel_H_
#define _VS_Kernel_H_

//#include "librc.h"
#include "toolbox/IO_ToolBox_Eigen.h"
#include "toolbox/Math_ToolBox.h"
#include "vsc_DataStructure.h"
#include "toolbox/VisGeo.h"
//#include "controller/CoordinateBuilder.h"

class Math_ToolBox;
class VS_Kernel: public Math_ToolBox, public VisGeo_PI
{
public:
	VS_Kernel(){}
	~VS_Kernel(){}

    /// Calculate the image Jacobian base on page 661 equation 47
	Eigen::MatrixXd  Calc_image_jacobian
	        (const Eigen::Matrix3d A, const Eigen::VectorXd uk, const Eigen::VectorXd vk, const Eigen::VectorXd image_depth);


    /// Calculate the image Jacobian base on page 663 equation 57
    Eigen::MatrixXd  match_pl
		(const Eigen::MatrixXd imgxy_objpt_a, const Eigen::MatrixXd imgxy_objpt_b,
		const Eigen::MatrixXd imgxy_tarpt_a, const Eigen::MatrixXd imgxy_tarpt_b,
		const Eigen::RowVectorXd imgz_objpt_a, const Eigen::RowVectorXd imgz_objpt_b,
		const Eigen::Matrix4d T_cr_a, const Eigen::Matrix4d T_cr_b, const Eigen::Matrix3d A_cama, const Eigen::Matrix3d A_camb);

    /// Calculate the image Jacobian base on page 661 equation 48
    Eigen::MatrixXd  match_pp
            (const Eigen::MatrixXd &imgxy_objpt_a, const Eigen::MatrixXd &imgxy_objpt_b,
             const Eigen::MatrixXd &imgxy_tarpt_a, const Eigen::MatrixXd &imgxy_tarpt_b,
             const Eigen::RowVectorXd &imgz_objpt_a, const Eigen::RowVectorXd &imgz_objpt_b,
              const Eigen::Matrix4d &T_cr_a, const Eigen::Matrix4d &T_cr_b,
               const Eigen::Matrix3d &A_cama, const Eigen::Matrix3d &A_camb);

    /// Repackage above functions
	Eigen::MatrixXd  match_pl  ( const vsc_ImgFeatureSet featureSet,  const Eigen::RowVectorXd imgz_objpt_a,
                             const Eigen::RowVectorXd imgz_objpt_b,  const vsc_EnvironVar env_var)
	{
		   return(  match_pl  ( featureSet.cur_A ,featureSet.cur_B  , featureSet.tar_A ,featureSet.tar_B,
									imgz_objpt_a, imgz_objpt_b,  env_var. T_cr_A,  env_var. T_cr_B, env_var. Intrinsc_A,env_var. Intrinsc_B)    );
	}

	Eigen::MatrixXd  match_pp  ( const vsc_ImgFeatureSet featureSet,  const Eigen::RowVectorXd imgz_objpt_a,
                             const Eigen::RowVectorXd imgz_objpt_b,  const vsc_EnvironVar env_var)
	{
		   return(  match_pp  ( featureSet.cur_A ,featureSet.cur_B  , featureSet.tar_A ,featureSet.tar_B,
									imgz_objpt_a, imgz_objpt_b,  env_var. T_cr_A,  env_var. T_cr_B, env_var. Intrinsc_A,env_var. Intrinsc_B)    );
	}

    /// Compute the velocity screw from camera coordinate to robot coordinate base on page 653 section B of paper
    Eigen::MatrixXd velocity_cam2robot(const Eigen::Matrix4d &T_cr);


   /// Extract the image jacobian and error vector from above result
   void Extract_Jimg_ef ( const Eigen::MatrixXd & Jimg_ef,  Eigen::VectorXd &ef,  Eigen::MatrixXd &J_img);

private:



};


#endif
