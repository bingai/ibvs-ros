#ifndef __DEPTHPREDICTOR_H__
#define __DEPTHPREDICTOR_H__
#include "Eigen/Eigen"
#include "Eigen/Dense"

#include "toolbox/VisGeo.h"
#include "vsc_DataStructure.h"

/*****************************
This class is designed for estimate roughly depth.
Based on 2 points in the image, here are some

******************************/

class DepthPredictor
{
public:
	DepthPredictor(){};
	~DepthPredictor(){};

    /// estimate the depth of the object by image feature
	Eigen::MatrixXd  Esti_Depth ( const vsc_ImgFeatureSet featureSet, const vsc_EnvironVar env_var,Eigen::MatrixXd & interSect_Pts_A,   Eigen::MatrixXd & interSect_Pts_B)
	{
		return (    Esti_Depth ( env_var.inv_intrinsic_A, env_var.inv_intrinsic_B, featureSet.cur_A,featureSet.cur_B,
									env_var.T_cA2cB,  interSect_Pts_A, interSect_Pts_B )   );
	}

	Eigen::MatrixXd  Esti_Depth ( const Eigen::Matrix3d inv_Intrinsic_A, const Eigen::Matrix3d inv_Intrinsic_B, const Eigen::MatrixXd Img_Pts_A, const Eigen::MatrixXd Img_Pts_B,
								  								const Eigen::MatrixXd T_camA2camB,  Eigen::MatrixXd & interSect_Pts_A,   Eigen::MatrixXd & interSect_Pts_B );

    // This function can estimate all points in a 2d-feature set. Output will be a 3d-feature set.
    void Esti_3d ( vsc_ImgFeatureSet & Points3d_Set, const vsc_ImgFeatureSet featureSet, const vsc_EnvironVar env_var );

private:
	bool gen_LineSeg_once ( const Eigen::MatrixXd Lines_byCol, Eigen::MatrixXd &  LineSegPoints_byCol,  int Point2Extrin_unit_ExchangeRate = 1000);

	double detect_Dist_btw_2Lines ( const Eigen::MatrixXd &  LineSegPoints_1, const Eigen::MatrixXd &  LineSegPoints_2,
																			Eigen::MatrixXd & NearestPts,    Eigen::MatrixXd & virtual_sect_point );
	double grab_SingleElemMat (Eigen::MatrixXd A);


private:
	VisGeo_PI vg;
	Math_ToolBox mtt;





};

#endif




