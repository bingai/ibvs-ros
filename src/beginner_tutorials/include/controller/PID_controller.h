#ifndef __PID_CONTROLLER_H__
#define __PID_CONTROLLER_H__
#include <iostream>
#include <stdio.h>
#include <cmath>
#include <assert.h>
#include <vector>

#include "Eigen/Eigen"
#include "Eigen/Dense"



#include "toolbox/VisGeo.h"
#include "toolbox/Math_ToolBox.h"
#include "controller/VS_Kernel.h"

class Math_ToolBox;
class PID_controller: public Math_ToolBox, public VisGeo_PI
{
	public:
	PID_controller(){};
	~PID_controller(){};
	/// P controller of PID contorl method, base on page 662 equation 55
    Eigen::VectorXd P_control(double Kp, const Eigen::VectorXd &ef, const Eigen::MatrixXd &J_img, double frame_rate);


    /// Convert the control input into robot command
    Eigen::MatrixXd dr2cmd (  const Eigen::MatrixXd T_tr, const Eigen::MatrixXd dr );


    //Eigen::MatrixXd dr_obj2cmd (    const Eigen::MatrixXd T_tr, const Eigen::MatrixXd T_cr,
    //                                const Eigen::MatrixXd T_oc_obj, const Eigen::MatrixXd dr_obj );
    //                                const Eigen::MatrixXd T_oc_obj, const Eigen::MatrixXd dr_obj );



};
#endif
