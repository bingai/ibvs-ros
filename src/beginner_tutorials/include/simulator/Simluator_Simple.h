#pragma once

#include <iostream>

#include "toolbox/VisGeo.h"
#include "vsc_DataStructure.h"
#include "toolbox/IO_ToolBox_Eigen.h"

class Simulator_Simple:
    public VisGeo_PI,public IO_ToolBox_Eigen
{
	public:
	Simulator_Simple(){};
	~Simulator_Simple(){};

	/*******************
	The parameters init_ed in this function are const.
	*******************/
	void Init_ReWorld  (
				const Eigen::MatrixXd &objpt_t__,
				const Eigen::MatrixXd &tarpt_r__,
				const Eigen::MatrixXd &cama_pos__,
				const Eigen::MatrixXd &camb_pos__,
				const Eigen::Matrix3d &A_cama__,
				const Eigen::Matrix3d &A_camb__ );

    void Init_ReWorld ( std::string DataSet_Path, const int VSmode);

	inline void print_init()
	{
		std::cout << "  this-> objpt_t  " <<std::endl << this-> objpt_t <<std::endl;
		std::cout << "  this-> tarpt_r  " <<std::endl << this-> tarpt_r <<std::endl;
		std::cout << "  this-> cama_pos  " <<std::endl << this-> cama_pos <<std::endl;
		std::cout << "  this-> camb_pos  " <<std::endl << this-> camb_pos <<std::endl;
		std::cout << "  this-> A_cama  " <<std::endl << this-> A_cama <<std::endl;
		std::cout << "  this-> A_camb  " <<std::endl << this-> A_camb <<std::endl;
	}

	/**************************
	When robot moves, where is the feature points on images.
	Input: Renew_world.
	output, new feature image coordinates. new Ttr
	*****************************/
	void Renew_World (const Eigen::MatrixXd & control_vec,  vsc_ImgFeatureSet & featureSet,   vsc_EnvironVar & env_var );

//=========================//
// Data Member:
//=========================//
				 Eigen::MatrixXd objpt_t;
				 Eigen::MatrixXd tarpt_r;
				 Eigen::MatrixXd cama_pos;
				 Eigen::MatrixXd camb_pos;
				 Eigen::Matrix3d A_cama;
				 Eigen::Matrix3d A_camb;

				 Eigen::MatrixXd  objpt_r;
                 Eigen::MatrixXd  objpt_ca;
                 Eigen::MatrixXd  objpt_cb;

                 Eigen::MatrixXd  T_tr;
};
