/**
@class Math_ToolBox
@brief some math library
@version v1.0
- Methods:
 -# convertFromHomo, convert homography point to normal point
 -# convertToHomo,   convert normal point to homography point
 -# rodrigues,       convert rotation vector to rotation matrix, vice versa
- Header List:
 -# iostream
 -# Eigen\Dense
- Modify Record:
-# v1.0-2013-4-25-wudan, TEST OK;
-# v.2.0 2014-1-16 MaLin: add skew_matrix, no TEST;
-# v.2.1 2014-1-16 MaLin: add matrix_minor, no TEST;
*/
#ifndef __MATH_TOOLBOX_H__
#define __MATH_TOOLBOX_H__
#include <iostream>
#include <assert.h>
#include <vector>
#include  "Eigen/Eigen"
#include  "Eigen/Dense"
#include "Eigen/LU"
#include "Eigen/SVD"

//#include "Eigen"
//#include "Dense"
//#include "Math_ToolBox_inl.h"
/*//zhz:What about translating this common class to a Template class like this:?
template <class matrixType ,int defMethParam > class Math_ToolBox
{
public:
	Math_ToolBox(void);
	~Math_ToolBox(void);

	matrixType convertFromHomo( const matrixType & sur, int method=defMethParam );
	matrixType convertToHomo( const matrixType & sur,int method=defMethParam);
	void rodrigues( Eigen::MatrixXd sur, Eigen::MatrixXd &dis );
	};
*/
class Math_ToolBox
{
public:
	Math_ToolBox(void);
	~Math_ToolBox(void);

	enum HomoMethod
	{
	    HOMO_ROW, HOMO_COL
	};
    // default is N-by-3, or N-by-4 pattern, so default homoMethod is  HOMO_COL
	Eigen::MatrixXd convertFromHomo( Eigen::MatrixXd sur, HomoMethod method );
	Eigen::MatrixXf convertFromHomo( Eigen::MatrixXf sur, HomoMethod method );

	Eigen::MatrixXd convertToHomo( Eigen::MatrixXd sur, HomoMethod method );
	Eigen::MatrixXf convertToHomo( Eigen::MatrixXf sur, HomoMethod method );

    void rodrigues( Eigen::MatrixXd sur, Eigen::MatrixXd &dis );
    Eigen::Matrix3d
    skew_matrix(const Eigen::Vector3d vec);

    Eigen::MatrixXd
    matrix_minor(const Eigen::MatrixXd &M,  int row, int col);

    Eigen::MatrixXd
    matrix_cofactor(const Eigen::MatrixXd &M);


	/// here is for Previous interface of calibration codes. Ensure calibration codes can work.
	//------------------------- dont touch--------------//
	HomoMethod change_method_adaptor ( const int method )
	{
	    assert( (method == 0) ||(method==1) );
        if (method == 0) return HOMO_ROW;
        if (method == 1) return HOMO_COL;
	}
	Eigen::MatrixXd convertFromHomo( Eigen::MatrixXd sur, int method )
	{
            convertFromHomo(sur,  change_method_adaptor(method));
	}
	Eigen::MatrixXf convertFromHomo( Eigen::MatrixXf sur, int method )
	{
	    convertFromHomo(sur,  change_method_adaptor(method));
	}

	Eigen::MatrixXd convertToHomo( Eigen::MatrixXd sur, int method )
	{
	    convertToHomo(sur,  change_method_adaptor(method));
	}
	Eigen::MatrixXf convertToHomo( Eigen::MatrixXf sur, int method )
	{
	    convertToHomo(sur,  change_method_adaptor(method));
	}
//-------------------------------------------------------------//
//  inline template

//-------------------------------------------------------------//
Eigen::MatrixXd  pinv(const Eigen::MatrixXd &M, const double tol);

#if 0
template<typename Scalar>
bool pinv (const Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &a,
           Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &a_pinv,
           double epsilon)
{
    if ( a.rows()<a.cols() )
    return false;
    //cout << "a in" <<endl<< a<<endl;
    // SVD
    Eigen::JacobiSVD< Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> > svdA(a,Eigen::ComputeFullU|Eigen::ComputeFullV);

    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> vSingular = svdA.singularValues();
    svdA.computeV();
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> AV =svdA.matrixV();
    svdA.computeU();
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> AU = svdA.matrixU();
    //// Build a diagonal matrix with the Inverted Singular values
    //// The pseudo inverted singular matrix is easy to compute :
    //// is formed by replacing every nonzero entry by its reciprocal (inversing).
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> vPseudoInvertedSingular(AV.cols(),1);

    for (int iRow =0; iRow<vSingular.rows(); iRow++)
    {
        if ( fabs(vSingular(iRow))<=epsilon ) // Todo : Put epsilon in parameter
            {
                vPseudoInvertedSingular(iRow,0)=0.;
            }
        else
            {
                vPseudoInvertedSingular(iRow,0)=1./vSingular(iRow);
            }
    }

    //// A little optimization here
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> mAdjointU = AU.adjoint().block(0,0,vSingular.rows(),AU.adjoint().cols());

    //// Pseudo-Inversion : V * S * U'
    a_pinv = (AV* vPseudoInvertedSingular.asDiagonal()) * mAdjointU ;

    return true;
} // template<typename Scalar>  bool pinv (;.........................)


Eigen::MatrixXd
multiRows(const Eigen::MatrixXd &A, const std::vector <int> selected_rows_ZeroBased);
Eigen::MatrixXd
multiCols(const Eigen::MatrixXd &A, const std::vector <int> selected_cols_ZeroBased);
Eigen::MatrixXd
anyBlock(const Eigen::MatrixXd &A,  const std::vector <int> selected_rows_ZeroBased ,const std::vector <int> selected_cols_ZeroBased);


#endif

};// class end

#endif

