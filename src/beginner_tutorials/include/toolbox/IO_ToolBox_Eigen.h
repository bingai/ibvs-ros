/**
@class IO_ToolBox_Eigen
@brief I/O functions base on Eigen
@version v1.0
- Methods��
 -# read_txt, read data in text
 -# write_txt, save data in text
 -# cast_comma, cast comma in string
- Header List��
 -# iostream
 -# Eigen\Dense
 -# fstream
- Dependencies:
-# Eigen
- Modify Record��
-# v1.0-2013-4-25-wudan
*/
#pragma once
#include <string>
#include <iostream>
#include <fstream>

#include  "Eigen/Eigen"
#include  "Eigen/Dense"




class IO_ToolBox_Eigen
{
public:
enum write_mode { out, app};
	IO_ToolBox_Eigen(void);
	~IO_ToolBox_Eigen(void);

	int read_txt(std::string adress,Eigen::MatrixXd &M,int row,int col);
	int read_txt(std::string adress, Eigen::MatrixXd &M);
	int read_txt(std::string adress,Eigen::MatrixXf &M,int row,int col);
	Eigen::MatrixXd read_txt(std::string adress)
	{
		Eigen::MatrixXd M;
		read_txt( adress, M);
		return M;
	}

	int write_txt(std::string adress,Eigen::MatrixXd sur,write_mode mode = out);
	int write_txt(std::string adress,Eigen::MatrixXf sur);

	int write_txt(std::string adress,double * input_array,int length);
	int write_txt( std::string adress,int * input_array,int length);

	int cast_comma(std::string &s);

	bool SaveNum (const std::string filename, const int val);

};

