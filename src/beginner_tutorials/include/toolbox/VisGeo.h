/*********************
This depends on:
1. eigen
2. Math_ToolBox

***********************/


#ifndef __VisGeo__H_
#define __VisGeo__H_




#include <math.h>
#include <assert.h>

#include <iostream>
#include  "Eigen/Eigen"
#include  "Eigen/Dense"
#include "toolbox/Math_ToolBox.h"

class Math_ToolBox; //forward declaration
class VisGeo_PI
{
public:
	VisGeo_PI(){};
	~VisGeo_PI(){};

    Math_ToolBox mt;

	enum RotationMatrixSelection
	{ RX,RY,RZ };

	/// Pose vector is 6x1 vector = [x; y; z; u; v; w] of a 6dof robot;
	/// Transformation matrix is 4x4 matrix = [R t; 0 1];
	/// which R = 3x3 rotaion matrix, and t = 3x1 translation vector

    /// Transform a 6x1 position vector into 4x4 transformation matrix
	bool Pose2T(const Eigen::MatrixXd PoseVect, Eigen::MatrixXd & Transformation );
	/// Transform a 4x4 transformation matrix into 6x1 position vector
	bool  T2Pose ( const  Eigen::MatrixXd & Transformation,  Eigen::MatrixXd & PoseVect);


	bool gen_RotationMatrix (const double angle_deg, Eigen::MatrixXd & rotation_matrix,RotationMatrixSelection key );
	bool gen_RotMat_3D (const double angleX_deg,const double angleY_deg,const double angleZ_deg,
						Eigen::MatrixXd & rotation_matrix);

	double calc_cos_sin (const double angle_deg, double & cosA,double & sinA);
	double deg2rad (const double deg);
	double rad2deg (const double rad);

    /// Transform a point in one coordinate into another coordinate
    bool TransformPoints ( const Eigen::MatrixXd T_matrix, const Eigen::MatrixXd PointsMat_byCol,
                          Eigen::MatrixXd & resultPointsMat );

    /// Project a 3D object points into 2D image points
   bool ProjectToImagePlane ( const Eigen::Matrix3d Intrinsic, const Eigen::MatrixXd PointsMat_byCol,
                          Eigen::MatrixXd & ImagePoints );

    /// Project a 2D image into 3D object points
	bool BackProject (  const Eigen::Matrix3d Intrinsic_inv,   const Eigen::MatrixXd ImagePoints,    Eigen::MatrixXd & PointsMat_byCol );

    /// Transform the image feature point from 2 cameras into 3D objects in robot coordinate
    bool FromTwoCamerasToRobot_3D ( Eigen::MatrixXd & Points3d_Robot, const Eigen::MatrixXd &T_cr_A, const Eigen::MatrixXd &T_cr_B,
                               const Eigen::MatrixXd  &Points3d_Cam_A, const Eigen::MatrixXd  &Points3d_Cam_B);

};





#endif
