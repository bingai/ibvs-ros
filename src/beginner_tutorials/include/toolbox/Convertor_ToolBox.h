#ifndef __CONVERTOR_TOOLBOX_H__
#define __CONVERTOR_TOOLBOX_H__

#define WITH_OPENCV
#define WITH_EIGEN

#include <vector>

#ifdef WITH_OPENCV
#include <opencv/cv.h>
#endif


#include "Eigen/Eigen"


class Convertor_ToolBox
{
public:
	std::vector<std::vector<double > >
	Eigen2vector(const Eigen::MatrixXd &objPts_in);

	Eigen::MatrixXd
	vector2Eigen(const std::vector<std::vector<double > >  &objPts_in);
#ifdef WITH_OPENCV
    std::vector < cv::Rect >
    Eigen2RectVector_xywh ( const Eigen::MatrixXd mat_row_is_xywh );
#endif
};




#endif
