
#pragma once

#include <iostream>
#include <stdio.h>
#include <cmath>
#include <vector>

#include  "Eigen/Eigen"
#include  "Eigen/Dense"

bool IsEqlMaxtrx (Eigen::MatrixXd A, Eigen::MatrixXd B, double tol = 1e-5);
void print_TestResult (bool sign);
bool calc_final_sign ( std::vector<bool>testResult, bool dispaly = false);
template  <typename T>  bool IsEql ( T A, T B, double tol = 1e-5 )
{
    T result = A - B;
	if ( result < tol)
		return true;
	else
		return false;
}
