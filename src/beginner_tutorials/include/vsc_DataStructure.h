#ifndef __VSC_DATASTRUCTURE_H__
#define __VSC_DATASTRUCTURE_H__
#include "Eigen/Eigen"
#include "Eigen/Dense"
#include <iostream>

struct vsc_EnvironVar
{
	Eigen::Matrix3d Intrinsc_A;
	Eigen::Matrix3d Intrinsc_B;
	Eigen::Matrix3d inv_intrinsic_A;
	Eigen::Matrix3d inv_intrinsic_B;
	Eigen::MatrixXd T_cr_A;
	Eigen::MatrixXd T_cr_B;
	Eigen::MatrixXd T_cA2cB;
	// added for one line to another line situation.
	Eigen::MatrixXd T_objPt_r;
	Eigen::MatrixXd T_tarPt_r;
	Eigen::MatrixXd T_oc_obj_a;
	Eigen::MatrixXd T_oc_obj_b;


	void print() const
	{
		std::cout << "  Intrinsc_A " <<std::endl << Intrinsc_A<<std::endl;
		std::cout << "  Intrinsc_B " <<std::endl << Intrinsc_B<<std::endl;
		std::cout << "  inv_intrinsic_A " <<std::endl << inv_intrinsic_A<<std::endl;
		std::cout << "  inv_intrinsic_B " <<std::endl << inv_intrinsic_B<<std::endl;
		std::cout << "  T_cr_A " <<std::endl << T_cr_A<<std::endl;
		std::cout << "  T_cr_B " <<std::endl << T_cr_B<<std::endl;
		std::cout << "  T_cA2cB " <<std::endl << T_cA2cB<<std::endl;
		std::cout << "  T_objPt_r " <<std::endl << T_objPt_r<<std::endl;
		std::cout << "  T_tarPt_r " <<std::endl << T_tarPt_r<<std::endl;
		std::cout << "  T_oc_obj_a " <<std::endl << T_oc_obj_a<<std::endl;
		std::cout << "  T_oc_obj_b " <<std::endl << T_oc_obj_b<<std::endl;

	}

};

struct vsc_ImgFeatureSet
{
	Eigen::MatrixXd tar_A ;
	Eigen::MatrixXd tar_B ;
	Eigen::MatrixXd cur_A ;
	Eigen::MatrixXd cur_B ;

    void set (const Eigen::MatrixXd _tar_A,
              const Eigen::MatrixXd _tar_B,
              const Eigen::MatrixXd _cur_A,
              const Eigen::MatrixXd _cur_B)
    {
        this->tar_A = _tar_A;
        this->tar_B = _tar_B;
        this->cur_A = _cur_A;
        this->cur_B = _cur_B;
    }

	void print() const
	{
		std::cout << "  tar_A " <<std::endl << tar_A<<std::endl;
		std::cout << "  tar_B " <<std::endl << tar_B<<std::endl;
		std::cout << "  cur_A " <<std::endl << cur_A<<std::endl;
		std::cout << "  cur_B " <<std::endl << cur_B<<std::endl;
	}

    bool dim_Check() const
	{
	    if(!((this->tar_A.rows() == this->tar_B.rows())&&
             (this->tar_B.rows() == this->cur_A.rows())&&
             (this->cur_A.rows() == this->cur_B.rows())))
        {
           return false;
        }
        if(!((this->tar_A.cols() == this->tar_B.cols())&&
             (this->tar_B.cols() == this->cur_A.cols())&&
             (this->cur_A.cols() == this->cur_B.cols())))
        {
           return false;
        }
	}
};
#endif